/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertFalse;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.IDEMIAConnectorConstants;
import com.alnt.connector.provisioning.services.IDEMIAConnectorServiceInterface;

/**
 * @author soori
 *
 */
public class GetAttributesTest {
	private Map<String, String> connectionParams = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams = new HashMap<String, String>();
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION));
		connectionParams.put(IDEMIAConnectorConstants.ALERT_APP_DATE_FORMAT, p.getProperty(IDEMIAConnectorConstants.ALERT_APP_DATE_FORMAT));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP, "");
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesTest() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		List response = connectionInterface.getAttributes();
		assertFalse(response.isEmpty());
	}

}
