/**
 * 
 */
package com.alnt.connector.provisioning;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.provisioning.model.IProvisioningStatus;
import com.alnt.connector.constants.IDEMIAConnectorConstants;
import com.alnt.connector.provisioning.services.IDEMIAConnectorServiceInterface;

/**
 * @author soori
 *
 */
public class ProvisioningStatusTest {
	private Map<String, String> connectionParams = null;
	String userImage = null;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		
		userImage = p.getProperty("userImage");
		connectionParams = new HashMap<String, String>();
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION));
		connectionParams.put(IDEMIAConnectorConstants.ALERT_APP_DATE_FORMAT, p.getProperty(IDEMIAConnectorConstants.ALERT_APP_DATE_FORMAT));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_REQUEST_FILE_LOCATION, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_REQUEST_FILE_LOCATION));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_RESPONSE_FILE_LOCATION, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_RESPONSE_FILE_LOCATION));
	}

	@Test
	public void createFile() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, String> userParameters = new HashMap<String, String>();
		// test files has 1104831,1104803,1104817
		userParameters.put(IDEMIAConnectorConstants.RECORD_SET_ID,"1104803");
		IProvisioningStatus response  =connectionInterface.getProvisioningStatus(userParameters);
		System.out.println(response.getParameters());
	}
	@Test
	public void unkownUserFile() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, String> userParameters = new HashMap<String, String>();
		userParameters.put(IDEMIAConnectorConstants.RECORD_SET_ID,"10417");
		IProvisioningStatus response  =connectionInterface.getProvisioningStatus(userParameters);
		System.out.println(response.getParameters());
	}
	
	@Test
	public void UserIdEmptyFile() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, String> userParameters = new HashMap<String, String>();
		userParameters.put(IDEMIAConnectorConstants.RECORD_SET_ID,"");
		IProvisioningStatus response  =connectionInterface.getProvisioningStatus(userParameters);
		System.out.println(response.getMessageDesc());
		System.out.println(response.getErrorCode());
	}
	
	@Test
	public void UserIdNullFile() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, String> userParameters = new HashMap<String, String>();
		userParameters.put("userId","adasd");
		IProvisioningStatus response  =connectionInterface.getProvisioningStatus(userParameters);
		System.out.println(response.getMessageDesc());
		System.out.println(response.getErrorCode());
	}

}
