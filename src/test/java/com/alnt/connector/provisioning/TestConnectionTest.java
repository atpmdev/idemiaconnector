/**
 * 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.IDEMIAConnectorConstants;
import com.alnt.connector.exception.IDEMIAConnectorException;
import com.alnt.connector.provisioning.services.IDEMIAConnectorServiceInterface;

/**
 * @author soori
 *
 */
public class TestConnectionTest {
	private Map<String, String> connectionParams = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP, null);
		
		connectionParams.put(IDEMIAConnectorConstants.SHOW_PROVISION_WARNINGS, "False");
		connectionParams.put(IDEMIAConnectorConstants.CUSTOM_ATTRIBUTES, "custom,custom2");
		connectionParams.put(IDEMIAConnectorConstants.SENSITIVE_ATTRIBUTES, "passWord");
	
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = IDEMIAConnectorException.class)
	public void nullConnectionParams() throws Exception {
		new IDEMIAConnectorServiceInterface(null);
	}

	@Test
	public void validCredentials() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		boolean testPassed = connectionInterface.testConnection();
		assertTrue(testPassed);
	}

	@Test
	public void wrongHost() throws Exception {
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST, "12.12.123.33");
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void wrongPassphrase() throws Exception {
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH, "True");
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE, "sdfsfsdf");
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void emptyEndpoint() throws Exception {
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST, "");
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	} 

	// TODO :: ideally this should fail , connector should have check for mandatory fields
	// too many test cases are skipped ( missing parameter , has parameter but value is null )
	
	@Test
	public void emptyMap() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(new HashMap<String, String>());
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

}
