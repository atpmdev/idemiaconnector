/**
 * 
 */
package com.alnt.connector.provisioning;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.alnt.connector.constants.IDEMIAConnectorConstants;
import com.alnt.connector.provisioning.services.IDEMIAConnectorServiceInterface;

/**
 * @author soori
 *
 */
public class CreateFileTest {
	private Map<String, String> connectionParams = null;
	String userImage = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));

		userImage = p.getProperty("userImage");
		connectionParams = new HashMap<String, String>();
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION));
		connectionParams.put(IDEMIAConnectorConstants.ALERT_APP_DATE_FORMAT, p.getProperty(IDEMIAConnectorConstants.ALERT_APP_DATE_FORMAT));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_REQUEST_FILE_LOCATION, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_REQUEST_FILE_LOCATION));
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_SFTP_RESPONSE_FILE_LOCATION, p.getProperty(IDEMIAConnectorConstants.CONN_PARAM_SFTP_RESPONSE_FILE_LOCATION));
	}

	@Test
	public void createFile() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		// User Id Attribute
		userParameters.put(IDEMIAConnectorConstants.RECORD_SET_ID, "12300123");
		// ********************************* Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_HEADER_LABEL, "PAMS20200225-065502");
		userParameters.put(IDEMIAConnectorConstants.FILE_DATE, "20200225121212");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME, "Wells Fargo Bank - Distribution Utility Center");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_CITY, "CHARLOTTE");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STATE, "NC");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP, "28269");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP_SFX, "");// #empty
		userParameters.put(IDEMIAConnectorConstants.CARD_REQUEST_COUNT, "407");
		// ********************************* Alternate Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_RECIPIENT_NAME, "MAUPIN, ANGELA");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET1, "MAC A0101-027");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET2, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_CITY, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STATE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP_SFX, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_MODE, "CARRIER");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_TYPE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.CARDTYPE, "5E"); // empty
		userParameters.put(IDEMIAConnectorConstants.SLA_TURNAROUND_TIME, ""); // empty
		// ********************************* Card Front Record ************************************/
		//userParameters.put(IDEMIAConnectorConstants.PICTURE,"p1407100-1105437.jpg");
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME, "RAMI");
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME_FONT, "17");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_FONT, "12");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME, "PACHECO");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE_FONT, "7");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE, "ALLIED UNIVERSAL SECURITY SERVICES");
		userParameters.put(IDEMIAConnectorConstants.SIGNATURE_FIELD, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_TEXT, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RANK, "Civilian");
		userParameters.put(IDEMIAConnectorConstants.PDF417_2D_BARCODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EMPLOYEE_AFFILIATION, "");// empty
		userParameters.put(IDEMIAConnectorConstants.HEADER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE1, "Wells Fargo");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.SEAL, "");// empty
		userParameters.put(IDEMIAConnectorConstants.FOOTER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.ISSUE_DATE, "20200225065502");
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.COLOR_BAR_CODING, "FCC60A");
		userParameters.put(IDEMIAConnectorConstants.COLOR_BORDER_CODING, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT1, "Vendor");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AFFILIATION_COLOR_CODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE_FIPS_201_2, "20241025065502");
		userParameters.put(IDEMIAConnectorConstants.ORGANIZATIONAL_AFFILIATION, "");// empty

		// ********************************* Card Back Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL, "1105300");
		userParameters.put(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION, "99999999");
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK1, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE3, "CHARLOTTE NC 28269");
		userParameters.put(IDEMIAConnectorConstants.HEIGHT, "");
		userParameters.put(IDEMIAConnectorConstants.EYES, "");
		userParameters.put(IDEMIAConnectorConstants.HAIR, "");
		userParameters.put(IDEMIAConnectorConstants.THREE_OF_9_BAR_CODE, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT2, "");
		userParameters.put(IDEMIAConnectorConstants.USER_IMAGE, extractBytes(userImage));
		connectionInterface.create(12321L, null, userParameters, null, null);
	}

	@Test
	@Ignore
	public void createFileWithoutUserImage() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		// User Id Attribute
		userParameters.put(IDEMIAConnectorConstants.RECORD_SET_ID, "1105437");
		// ********************************* Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_HEADER_LABEL, "PAMS20200225-065502");
		userParameters.put(IDEMIAConnectorConstants.FILE_DATE, "20200225121212");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME, "Wells Fargo Bank - Distribution Utility Center");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_CITY, "CHARLOTTE");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STATE, "NC");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP, "28269");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP_SFX, "");// #empty
		userParameters.put(IDEMIAConnectorConstants.CARD_REQUEST_COUNT, "407");
		// ********************************* Alternate Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_RECIPIENT_NAME, "MAUPIN, ANGELA");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET1, "MAC A0101-027");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET2, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_CITY, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STATE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP_SFX, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_MODE, "CARRIER");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_TYPE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.CARDTYPE, "5E"); // empty
		userParameters.put(IDEMIAConnectorConstants.SLA_TURNAROUND_TIME, ""); // empty
		// ********************************* Card Front Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.PICTURE, "p1407100-1105437.jpg");
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME, "RAMI");
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME_FONT, "17");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_FONT, "12");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME, "PACHECO");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE_FONT, "7");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE, "ALLIED UNIVERSAL SECURITY SERVICES");
		userParameters.put(IDEMIAConnectorConstants.SIGNATURE_FIELD, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_TEXT, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RANK, "Civilian");
		userParameters.put(IDEMIAConnectorConstants.PDF417_2D_BARCODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EMPLOYEE_AFFILIATION, "");// empty
		userParameters.put(IDEMIAConnectorConstants.HEADER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE1, "Wells Fargo");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.SEAL, "");// empty
		userParameters.put(IDEMIAConnectorConstants.FOOTER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.ISSUE_DATE, "20200225065502");
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.COLOR_BAR_CODING, "FCC60A");
		userParameters.put(IDEMIAConnectorConstants.COLOR_BORDER_CODING, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT1, "Vendor");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AFFILIATION_COLOR_CODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE_FIPS_201_2, "20241025065502");
		userParameters.put(IDEMIAConnectorConstants.ORGANIZATIONAL_AFFILIATION, "");// empty

		// ********************************* Card Back Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL, "1105300");
		userParameters.put(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION, "99999999");
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK1, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE3, "CHARLOTTE NC 28269");
		userParameters.put(IDEMIAConnectorConstants.HEIGHT, "");
		userParameters.put(IDEMIAConnectorConstants.EYES, "");
		userParameters.put(IDEMIAConnectorConstants.HAIR, "");
		userParameters.put(IDEMIAConnectorConstants.THREE_OF_9_BAR_CODE, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT2, "");
		connectionInterface.create(12321L, null, userParameters, null, null);
	}

	@Test
	@Ignore
	public void createFileWithoutUserImageFileName() throws Exception {
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		// User Id Attribute
		userParameters.put(IDEMIAConnectorConstants.RECORD_SET_ID, "1105437");
		// ********************************* Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_HEADER_LABEL, "PAMS20200225-065502");
		userParameters.put(IDEMIAConnectorConstants.FILE_DATE, "20200225121212");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME, "Wells Fargo Bank - Distribution Utility Center");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_CITY, "CHARLOTTE");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STATE, "NC");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP, "28269");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP_SFX, "");// #empty
		userParameters.put(IDEMIAConnectorConstants.CARD_REQUEST_COUNT, "407");
		// ********************************* Alternate Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_RECIPIENT_NAME, "MAUPIN, ANGELA");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET1, "MAC A0101-027");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET2, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_CITY, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STATE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP_SFX, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_MODE, "CARRIER");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_TYPE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.CARDTYPE, "5E"); // empty
		userParameters.put(IDEMIAConnectorConstants.SLA_TURNAROUND_TIME, ""); // empty
		// ********************************* Card Front Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME, "RAMI");
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME_FONT, "17");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_FONT, "12");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME, "PACHECO");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE_FONT, "7");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE, "ALLIED UNIVERSAL SECURITY SERVICES");
		userParameters.put(IDEMIAConnectorConstants.SIGNATURE_FIELD, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_TEXT, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RANK, "Civilian");
		userParameters.put(IDEMIAConnectorConstants.PDF417_2D_BARCODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EMPLOYEE_AFFILIATION, "");// empty
		userParameters.put(IDEMIAConnectorConstants.HEADER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE1, "Wells Fargo");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.SEAL, "");// empty
		userParameters.put(IDEMIAConnectorConstants.FOOTER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.ISSUE_DATE, "20200225065502");
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.COLOR_BAR_CODING, "FCC60A");
		userParameters.put(IDEMIAConnectorConstants.COLOR_BORDER_CODING, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT1, "Vendor");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AFFILIATION_COLOR_CODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE_FIPS_201_2, "20241025065502");
		userParameters.put(IDEMIAConnectorConstants.ORGANIZATIONAL_AFFILIATION, "");// empty

		// ********************************* Card Back Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL, "1105300");
		userParameters.put(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION, "99999999");
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK1, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE3, "CHARLOTTE NC 28269");
		userParameters.put(IDEMIAConnectorConstants.HEIGHT, "");
		userParameters.put(IDEMIAConnectorConstants.EYES, "");
		userParameters.put(IDEMIAConnectorConstants.HAIR, "");
		userParameters.put(IDEMIAConnectorConstants.THREE_OF_9_BAR_CODE, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT2, "");
		userParameters.put(IDEMIAConnectorConstants.USER_IMAGE, extractBytes(userImage));
		connectionInterface.create(12321L, null, userParameters, null, null);
	}

	@Test
	@Ignore
	public void createFileWrongtUserImageFilepath() throws Exception {
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION, "z://");
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		// User Id Attribute
		userParameters.put(IDEMIAConnectorConstants.RECORD_SET_ID, "1105437");
		// ********************************* Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_HEADER_LABEL, "PAMS20200225-065502");
		userParameters.put(IDEMIAConnectorConstants.FILE_DATE, "20200225121212");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME, "Wells Fargo Bank - Distribution Utility Center");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_CITY, "CHARLOTTE");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STATE, "NC");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP, "28269");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP_SFX, "");// #empty
		userParameters.put(IDEMIAConnectorConstants.CARD_REQUEST_COUNT, "407");
		// ********************************* Alternate Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_RECIPIENT_NAME, "MAUPIN, ANGELA");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET1, "MAC A0101-027");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET2, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_CITY, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STATE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP_SFX, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_MODE, "CARRIER");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_TYPE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.CARDTYPE, "5E"); // empty
		userParameters.put(IDEMIAConnectorConstants.SLA_TURNAROUND_TIME, ""); // empty
		// ********************************* Card Front Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME, "RAMI");
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME_FONT, "17");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_FONT, "12");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME, "PACHECO");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE_FONT, "7");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE, "ALLIED UNIVERSAL SECURITY SERVICES");
		userParameters.put(IDEMIAConnectorConstants.SIGNATURE_FIELD, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_TEXT, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RANK, "Civilian");
		userParameters.put(IDEMIAConnectorConstants.PDF417_2D_BARCODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EMPLOYEE_AFFILIATION, "");// empty
		userParameters.put(IDEMIAConnectorConstants.HEADER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE1, "Wells Fargo");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.SEAL, "");// empty
		userParameters.put(IDEMIAConnectorConstants.FOOTER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.ISSUE_DATE, "20200225065502");
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.COLOR_BAR_CODING, "FCC60A");
		userParameters.put(IDEMIAConnectorConstants.COLOR_BORDER_CODING, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT1, "Vendor");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AFFILIATION_COLOR_CODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE_FIPS_201_2, "20241025065502");
		userParameters.put(IDEMIAConnectorConstants.ORGANIZATIONAL_AFFILIATION, "");// empty

		// ********************************* Card Back Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL, "1105300");
		userParameters.put(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION, "99999999");
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK1, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE3, "CHARLOTTE NC 28269");
		userParameters.put(IDEMIAConnectorConstants.HEIGHT, "");
		userParameters.put(IDEMIAConnectorConstants.EYES, "");
		userParameters.put(IDEMIAConnectorConstants.HAIR, "");
		userParameters.put(IDEMIAConnectorConstants.THREE_OF_9_BAR_CODE, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT2, "");
		userParameters.put(IDEMIAConnectorConstants.USER_IMAGE, extractBytes(userImage));
		connectionInterface.create(12321L, null, userParameters, null, null);
	}

	@Test
	@Ignore
	public void createFileInvalidDate() throws Exception {
		connectionParams.put(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION, "X:\\");
		IDEMIAConnectorServiceInterface connectionInterface = new IDEMIAConnectorServiceInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		// User Id Attribute
		userParameters.put(IDEMIAConnectorConstants.RECORD_SET_ID, "1105437");
		// ********************************* Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_HEADER_LABEL, "PAMS20200225-065502");
		userParameters.put(IDEMIAConnectorConstants.FILE_DATE, "20200225121212ff");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME, "Wells Fargo Bank - Distribution Utility Center");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STREET2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_CITY, "CHARLOTTE");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_STATE, "NC");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP, "28269");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_ZIP_SFX, "");// #empty
		userParameters.put(IDEMIAConnectorConstants.CARD_REQUEST_COUNT, "407");
		// ********************************* Alternate Header Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_RECIPIENT_NAME, "MAUPIN, ANGELA");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_PHONE_NUMBER, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET1, "MAC A0101-027");
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET2, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_CITY, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STATE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP_SFX, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_MODE, "CARRIER");
		userParameters.put(IDEMIAConnectorConstants.SHIPPING_TYPE, ""); // empty
		userParameters.put(IDEMIAConnectorConstants.CARDTYPE, "5E"); // empty
		userParameters.put(IDEMIAConnectorConstants.SLA_TURNAROUND_TIME, ""); // empty
		// ********************************* Card Front Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME, "RAMI");
		userParameters.put(IDEMIAConnectorConstants.LAST_NAME_FONT, "17");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_FONT, "12");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME, "PACHECO");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE_FONT, "7");
		userParameters.put(IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE, "ALLIED UNIVERSAL SECURITY SERVICES");
		userParameters.put(IDEMIAConnectorConstants.SIGNATURE_FIELD, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_TEXT, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RANK, "Civilian");
		userParameters.put(IDEMIAConnectorConstants.PDF417_2D_BARCODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EMPLOYEE_AFFILIATION, "");// empty
		userParameters.put(IDEMIAConnectorConstants.HEADER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE1, "Wells Fargo");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_LINE2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.SEAL, "");// empty
		userParameters.put(IDEMIAConnectorConstants.FOOTER, "");// empty
		userParameters.put(IDEMIAConnectorConstants.ISSUE_DATE, "20200225065502");
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.COLOR_BAR_CODING, "FCC60A");
		userParameters.put(IDEMIAConnectorConstants.COLOR_BORDER_CODING, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT1, "Vendor");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.AFFILIATION_COLOR_CODE, "");// empty
		userParameters.put(IDEMIAConnectorConstants.EXPIRATION_DATE_FIPS_201_2, "20241025065502");
		userParameters.put(IDEMIAConnectorConstants.ORGANIZATIONAL_AFFILIATION, "");// empty

		// ********************************* Card Back Record ************************************/
		userParameters.put(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL, "1105300");
		userParameters.put(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION, "99999999");
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK1, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK2, "");// empty
		userParameters.put(IDEMIAConnectorConstants.MAG_STRIPE_TRACK3, "");// empty
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE1, "Attn: Daisy Zeigler-Wrenn");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE2, "12301 Vance Davis Drive");
		userParameters.put(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE3, "CHARLOTTE NC 28269");
		userParameters.put(IDEMIAConnectorConstants.HEIGHT, "");
		userParameters.put(IDEMIAConnectorConstants.EYES, "");
		userParameters.put(IDEMIAConnectorConstants.HAIR, "");
		userParameters.put(IDEMIAConnectorConstants.THREE_OF_9_BAR_CODE, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1, "");
		userParameters.put(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT2, "");
		userParameters.put(IDEMIAConnectorConstants.USER_IMAGE, extractBytes(userImage));
		connectionInterface.create(12321L, null, userParameters, null, null);
	}

	private byte[] extractBytes(String ImageName) throws IOException {
		BufferedImage bImage = ImageIO.read(new File(ImageName));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(bImage, "jpg", bos);
		return bos.toByteArray();
	}

}
