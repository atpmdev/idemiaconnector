package com.alnt.connector.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.log4j.Logger;

public class FlatFileWriter {

	PrintWriter writer = null;
	File file;
	private static final String CLASS_NAME = "com.alnt.fileconnector.common.FlatFileWriter";
	public static Logger logger = Logger.getLogger(CLASS_NAME);

	public boolean initialize(String filePath) {
		boolean writerCreated = true;
		file = new File(filePath);
		if (!file.exists()) {
			try {
				boolean fileCreated = file.createNewFile();
				if (!fileCreated) {
					writerCreated = false;
					// logWarning(METHOD_NAME,"File not found:"+filePath);
				}
			} catch (IOException io) {
				writerCreated = false;
				logger.warn("File not created:" + filePath);
			}

		}
		// else{
		try {
			writer = new PrintWriter(new FileWriter(file, true));

//				int i=1;
//				while((i++)<=linesToSkip){
//					firstLine = writer.
//				}
		} catch (FileNotFoundException e) {
			logger.error(CLASS_NAME + e);
			return false;
		} catch (IOException e) {
			logger.error(CLASS_NAME + e);
			return false;
		}
		// }

		return writerCreated;
	}

	public boolean writeLine(String line) {
		try {
			if (line != null)
				writer.println(line);
		} catch (Exception e) {
			logger.error(CLASS_NAME + "Error while writing data " + e);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean flush() {

		try {
			writer.flush();
		} catch (Exception e) {
			logger.error(CLASS_NAME + "Error while flushing data " + e);
			// e.printStackTrace();
			return false;
		}

		return true;
	}

	public void writeLines(List<String> lines) throws IOException {
		if (lines != null && !lines.isEmpty()) {
			for (String line : lines) {
				writeLine(line);
			}
		}
	}

	public boolean close() {

		String METHOD_NAME = "close";

		boolean isClosed = true;

		try {
			writer.close();
		} catch (Exception e) {
			logger.error(CLASS_NAME + "Error in " + METHOD_NAME);
			return false;
		}

		return isClosed;

	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

}
