
/*  Purpose of this program is to provide an API that generates a IDEMIA PIV input file
 * 
 */

// 
//

package com.alnt.connector.helper;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * 
 */
public class pifgen {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ByteArrayOutputStream bosLow = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(bosLow);

		pifgen pifgen = new pifgen();
		if (pifgen.genSample(ps, "SOORI.txt")) {
			ps.println("Completed.");
		} else {
			ps.println("PREMATURE EXIT!");
		}

		System.out.printf(bosLow.toString());
	}

	long record, recordSetId;

	public boolean genSample(PrintStream ps, String filename) {
		try {
			FileOutputStream outputStream = new FileOutputStream(filename);
			PrintStream fileps = new PrintStream(outputStream);
			CardRequestCount = 1;
			recordSetId = 1000;
			genHeader(fileps);
			genFront(fileps);
			genBack(fileps);
			recordSetId++;
			outputStream.close();

		} catch (IOException e) {
			ps.println(e.toString());
			return false;
		}

		return true;
	}

//--------------------------------------

	public String AgencyHeaderLabel, FileDate, Recipient_Name, Phone_Number, Street_1, Street_2, City, State, Zip, Zip_Sfx;
	public long CardRequestCount;

	public void sampleHeaderData() {
		AgencyHeaderLabel = "B1234";
		FileDate = "";
		Recipient_Name = "Horace Muffin";
		Phone_Number = "654-333-4444";
		Street_1 = "The Friendly Buffalo";
		Street_2 = "16722 198th Ave";
		City = "Big Lake";
		State = "MN";
		Zip = "55309";
		Zip_Sfx = "4714";
	}

	public void genHeader(PrintStream fileps) {
		sampleHeaderData();
		fileps.print("00000000" + "|" + AgencyHeaderLabel + "|" + FileDate + "|");
		fileps.print(Recipient_Name + "|" + Phone_Number + "|" + Street_1 + "|" + Street_2 + "|" + City + "|" + State + "|" + Zip + "|" + Zip_Sfx + "|");
		fileps.println(CardRequestCount);
		record = 1;
	}

//--------------------------------------

	public String FacialImageFilename, lastName, firstMiddleInitial, signatureField, agencySpecificText, rank, pdf417_2D_Barcode, EmployeeAffiliation, Header, AgencySpecific_1, AgencySpecific_2, Seal, Footer, IssueDate, ExpirationDate,
			ColorBarCoding, ColorBorderCoding, AgencySpecific_3, AgencySpecific_4, AgencySpecific_5, Affiliation_Color_Code, Expiration_Date2, Organizational_Affiliation;

	void sampleFrontData() {

		switch ((int) recordSetId) {
		default:
		case 1000:
			FacialImageFilename = "fred.jpg";
			lastName = "Flintstone";
			firstMiddleInitial = "Fred";
			break;

		case 1001:
			FacialImageFilename = "barn.jpg";
			lastName = "Rubble";
			firstMiddleInitial = "Barney J";
			break;

		case 1002:
			FacialImageFilename = "soori.jpg";
			lastName = "Reddy";
			firstMiddleInitial = "Surender Reddy";
		}

		Header = "Slate Mining";
		Seal = "org_seal.jpg";
		ColorBarCoding = "FFFFAA";
		ColorBorderCoding = "FFFFAA";
		Expiration_Date2 = "AUG2022";

		signatureField = "";
		agencySpecificText = "";
		rank = "";
		pdf417_2D_Barcode = "";
		EmployeeAffiliation = "";
		AgencySpecific_1 = "";
		AgencySpecific_2 = "";
		Footer = "";
		IssueDate = "";
		ExpirationDate = "";
		AgencySpecific_3 = "";
		AgencySpecific_4 = "";
		AgencySpecific_5 = "";
		Affiliation_Color_Code = "";
		Organizational_Affiliation = "";
	}

	public void recordAndRecordSet(PrintStream fileps) {
		fileps.format("%1$08d", record);
		fileps.print("|");
		fileps.format("%1$08d", recordSetId);
		fileps.print("|");

		record++;
	}

	public void genFront(PrintStream fileps) {
		sampleFrontData();
		recordAndRecordSet(fileps);
		fileps.println("68A1" + "|" + FacialImageFilename + "|" + lastName + ";" + firstMiddleInitial + "|" + signatureField + "|" + agencySpecificText + "|" + rank + "|" + pdf417_2D_Barcode + "|" + EmployeeAffiliation + "|" + Header + "|"
				+ AgencySpecific_1 + ";" + AgencySpecific_2 + "|" + Seal + "|" + Footer + "|" + IssueDate + "|" + ExpirationDate + "|" + ColorBarCoding + "|" + ColorBorderCoding + "|" + AgencySpecific_3 + ";" + AgencySpecific_4 + ";"
				+ AgencySpecific_5 + "|" + Affiliation_Color_Code + "|" + Expiration_Date2 + "|" + Organizational_Affiliation);
	}

//--------------------------------------

	public String Agency_Card_Serial_Number, Issuer_Identification_Number, MagStripe_Track_1, MagStripe_Track_2, MagStripe_Track_3, Return_Address_line_1, Return_Address_line_2, Return_Address_line_3, Height, Eyes, Hair, BarCode_3_of_9,
			Agency_specific_text_back_1, Agency_specific_text_back_2;

	public void sampleBackData() {
		Issuer_Identification_Number = "99999999";
		Agency_Card_Serial_Number = "12345678";
		Height = "5.11";
		Eyes = "Blue";
		Hair = "Brown";
		MagStripe_Track_1 = "";
		MagStripe_Track_2 = "";
		MagStripe_Track_3 = "";
		Return_Address_line_1 = "";
		Return_Address_line_2 = "";
		Return_Address_line_3 = "";
		BarCode_3_of_9 = "";
		Agency_specific_text_back_1 = "";
		Agency_specific_text_back_2 = "";
	}

	public void genBack(PrintStream fileps) {
		sampleBackData();
		recordAndRecordSet(fileps);
		fileps.println("68A2" + "|" + Agency_Card_Serial_Number + "|" + Issuer_Identification_Number + "|" + MagStripe_Track_1 + ";" + MagStripe_Track_2 + ";" + MagStripe_Track_3 + "|" + Return_Address_line_1 + ";" + Return_Address_line_2
				+ ";" + Return_Address_line_3 + "|" + Height + ";" + Eyes + ";" + Hair + "|" + BarCode_3_of_9 + "|" + Agency_specific_text_back_1 + "|" + Agency_specific_text_back_2);
	}

//--------------------------------------

}
