package com.alnt.connector.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.PublicKeyDataDecryptorFactory;
import org.bouncycastle.openpgp.operator.bc.BcPublicKeyDataDecryptorFactory;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;

/**
 * Taken from org.bouncycastle.openpgp.examples
 *
 * @author seamans
 *
 */
public class PGPUtility {

	public static String CLASS_NAME = PGPUtility.class.getName();
	public static Logger logger = Logger.getLogger(CLASS_NAME);


	
	/**
	 * Load a secret key ring collection from keyIn and find the secret key corresponding to
	 * keyID if it exists.
	 *
	 * @param keyIn input stream representing a key ring collection.
	 * @param keyID keyID we want.
	 * @param pass passphrase to decrypt secret key with.
	 * @return
	 * @throws IOException
	 * @throws PGPException
	 * @throws NoSuchProviderException
	 */
	private static PGPPrivateKey findSecretKey(InputStream keyIn, long keyID, byte[] pass)
			throws IOException, PGPException, NoSuchProviderException
	{
		
		PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(keyIn), new JcaKeyFingerprintCalculator());

		PGPSecretKey pgpSecKey = pgpSec.getSecretKey(keyID);

		if (pgpSecKey == null) {
			return null;
		}
		String str = new String(pass); 
		return  decryptArmoredPrivateKey(pgpSecKey, str);
		//        return pgpSecKey.extractPrivateKey(pass, "BC");
	}

	static PGPPrivateKey decryptArmoredPrivateKey(PGPSecretKey secretKey, String password) throws IOException, PGPException {
		return  secretKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder().setProvider("BC").build(password.toCharArray()));
	}

	/**
	 * decrypt the passed in message stream
	 */
	@SuppressWarnings("unchecked")
	public static void decryptFile(InputStream in, OutputStream out, InputStream keyIn, byte[] passwd)
			throws Exception
	{

		String methodName = "decrypt";
		logger.debug(CLASS_NAME + methodName + " Started...");


		Security.addProvider(new BouncyCastleProvider());

		in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);

		PGPObjectFactory pgpF = new PGPObjectFactory(PGPUtil.getDecoderStream( in ), //TODO: Changed
				new JcaKeyFingerprintCalculator() );

		PGPEncryptedDataList enc;

		Object o = pgpF.nextObject();
		//
		// the first object might be a PGP marker packet.
		//
		if (o instanceof  PGPEncryptedDataList) {
			enc = (PGPEncryptedDataList) o;
		} else {
			enc = (PGPEncryptedDataList) pgpF.nextObject();
		}

		//
		// find the secret key
		//
		Iterator<PGPPublicKeyEncryptedData> it = enc.getEncryptedDataObjects();
		PGPPrivateKey sKey = null;
		PGPPublicKeyEncryptedData pbe = null;

		while (sKey == null && it.hasNext()) {
			pbe = it.next();

			sKey = findSecretKey(keyIn, pbe.getKeyID(), passwd);
		}

		if (sKey == null) {
			throw new IllegalArgumentException("Secret key for message not found.");
		}

		PublicKeyDataDecryptorFactory decryptorFactory=new BcPublicKeyDataDecryptorFactory(sKey); 
		InputStream clear = pbe.getDataStream(decryptorFactory);


		PGPObjectFactory plainFact = new PGPObjectFactory(clear, null); //TODO

		Object message = plainFact.nextObject();

		if (message instanceof  PGPCompressedData) {
			PGPCompressedData cData = (PGPCompressedData) message;
			PGPObjectFactory pgpFact = new PGPObjectFactory(cData.getDataStream(), null);//TODO

			message = pgpFact.nextObject();
		}

		if (message instanceof  PGPLiteralData) {
			PGPLiteralData ld = (PGPLiteralData) message;

			InputStream unc = ld.getInputStream();
			int ch;

			while ((ch = unc.read()) >= 0) {
				out.write(ch);
			}
		} else if (message instanceof  PGPOnePassSignatureList) {
			throw new PGPException("Encrypted message contains a signed message - not literal data.");
		} else {
			throw new PGPException("Message is not a simple encrypted file - type unknown.");
		}

		if (pbe.isIntegrityProtected()) {
			if (!pbe.verify()) {
				throw new PGPException("Message failed integrity check");
			}
		}

		logger.debug(CLASS_NAME + methodName + " END.");

	}

	

	//get Secret Key Text from File
	public static  String getSecretKeyText(String privateKeyTextFilePath) throws Exception{
		String methodName = "getSecretKeyText";
		logger.debug(CLASS_NAME + methodName + " Started...");

		String keyText = "";
		File file = new File(privateKeyTextFilePath);  //"private.secret.Key.txt"
		byte[] bArray = readFileToByteArray(file);
		keyText = new String(bArray);
		//displaying content of byte array
//		for (int i = 0; i < bArray.length; i++){
//			System.out.print((char) bArray[i]);
//		}
		logger.debug(CLASS_NAME + methodName + " END.");
		return keyText;
	}

	private static  byte[] readFileToByteArray(File file) throws IOException,Exception{
		
		String methodName = "readFileToByteArray";
		logger.debug(CLASS_NAME + methodName + " Started...");
		
		FileInputStream fis = null;
		// Creating a byte array using the length of the file
		// file.length returns long which is cast to int
		byte[] bArray = new byte[(int) file.length()];

		fis = new FileInputStream(file);
		fis.read(bArray);
		fis.close();        
		logger.debug(CLASS_NAME + methodName + " END.");

		return bArray;
	}


	public static boolean decrypt(String sourceFilePath,String destinationFilePath,
			String privateKeyFilePath,String privateKeyTextFilePath) throws Exception {
		String methodName = "decrypt";
		logger.debug(CLASS_NAME + methodName + "File Decryption Started...");

		boolean isSuccess = false;

		if(sourceFilePath==null || sourceFilePath.isEmpty()){
			throw new Exception("Source File Path is not provided!!");
		}

		if(privateKeyFilePath==null || privateKeyFilePath.isEmpty()){
			throw new Exception("Private Key File Path is not provided!!");
		}
		if(destinationFilePath==null || destinationFilePath.isEmpty()){
			throw new Exception("Destination File Path is not provided!!");
		}
		if(privateKeyTextFilePath==null || privateKeyTextFilePath.isEmpty()){
			throw new Exception("Private key TEXT File Path is not provided!!");
		}

		FileInputStream in = new FileInputStream(sourceFilePath); //"EncryptedUsers.csv"
		FileInputStream keyIn = new FileInputStream(privateKeyFilePath); //"amfam.private.cert.0214A56AA70F77B26EB1CFB4B11102722E394370.asc"
		FileOutputStream out = new FileOutputStream(destinationFilePath); //"DecryptedUsers.csv"
		String passphrase = getSecretKeyText(privateKeyTextFilePath);
		PGPUtility.decryptFile(in, out, keyIn, passphrase.getBytes());
		in.close();
		out.close();
		keyIn.close();
		isSuccess=true;

		logger.debug(CLASS_NAME + methodName + "File Decryption End");
		return isSuccess;
	}

}