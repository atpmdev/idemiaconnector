package com.alnt.connector.helper;

import java.util.Properties;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class IDEMIAClientHelper {

	final private static String CLASS_NAME = IDEMIAClientHelper.class.getName();
	static Logger logger = Logger.getLogger(CLASS_NAME);

	public static Channel getSFTPConnection(final String ftpHostName, final String ftpUserName, final String ftpPassword, final int ftpPortNumber,  final String privateKeyFilePath, final String passphrase,
			boolean isSSHAuthentication) {
		if ( isSSHAuthentication) {
			return getSFTPConnection(ftpHostName, ftpUserName, ftpPortNumber, privateKeyFilePath, passphrase, null);
		} else if ( !isSSHAuthentication) {
			return getSFTPConnection(ftpHostName, ftpUserName, ftpPortNumber, null, null, ftpPassword);
		}
		return null;
	}

	public static FTPClient getFTPConnection(final String ftpHostName, final String ftpUserName, final String ftpPassword, final int ftpPortNumber, final boolean isSecure) {
		FTPClient fTPClient = null;
		fTPClient = getFTPConnectionClient(ftpHostName, ftpUserName, ftpPassword, ftpPortNumber ,isSecure);
		return fTPClient;
	}

	private static FTPClient getFTPConnectionClient(String ftpHostName, String ftpUserName, String ftpPassword, int ftpPortNumber,boolean isSecure) {
		final String METHOD_NAME = "getFTPConnectionClient()";
		FTPClient ftp = null;
		if(isSecure) {
			ftp = new FTPSClient();
		}else {
			ftp = new FTPClient();
		}
		try {
			ftp.setControlEncoding("UTF-8");
			ftp.connect(ftpHostName);
			boolean isSuccess=ftp.login(ftpUserName, ftpPassword);
			if(!isSuccess) {
			    logger.error("Login Failed, Please check FTP Login credentials!!");
			}else {
			ftp.enterLocalPassiveMode(); // important!
			return ftp;
			}
		}catch (Exception e) {
			logger.error(CLASS_NAME +" "+METHOD_NAME+" "+ "exception in FTP file delete operation--> :", e);
		}
		return null;
	}

	private static ChannelSftp getSFTPConnection(String ftpHostName, String ftpUserName, int ftpPortNumber, String privateKeyFilePath, String passphrase, String ftpPassword) {
		final String METHOD_NAME = "getSFTPConnection()";
		logger.info(CLASS_NAME + " getSFTPConnection(): Start of method");
		JSch jsch = new JSch();
		ChannelSftp channel = null;
		try {
			Session session = jsch.getSession(ftpUserName, ftpHostName, ftpPortNumber);
			if (null != privateKeyFilePath) {
				jsch.addIdentity(privateKeyFilePath, passphrase);
				// session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
			} else {
				session.setPassword(ftpPassword);
			}
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();
		} catch (JSchException ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		return channel;
	}

}
