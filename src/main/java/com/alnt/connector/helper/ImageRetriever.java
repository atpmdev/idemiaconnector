package com.alnt.connector.helper;

import static com.alnt.connector.helper.CommonUtil.isEmpty;
import static javax.imageio.ImageIO.createImageInputStream;
import static javax.imageio.ImageIO.getImageReaders;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

public class ImageRetriever {

	public static String CLASS_NAME = ImageRetriever.class.getName();
	public static Logger logger = Logger.getLogger(CLASS_NAME);

	public static final String DEFAULT_SANITIZED_PATTERN = "[^a-zA-Z0-9\\/\\\\:._ ~]";
	private static final Integer DEFAULT_MAX_FILESIZE_KB = 10;//Default 10KB
	private static final String DEFAULT_JPG = "jpg";

	private final Integer maxFileSizeInKB; // in kiloBytes
	private final String basePath;
	private final String sanitizingPattern;
	private boolean base64EncodingNeeded = false;

	public ImageRetriever(final Object basePath) {
		this.basePath =  (String) (isEmpty(basePath) ? "": basePath);
		this.maxFileSizeInKB =  DEFAULT_MAX_FILESIZE_KB;

		//FIXME- try using the ESAPI validator approach instead for sanitizing...
		this.sanitizingPattern = DEFAULT_SANITIZED_PATTERN;
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " constructor " + " basePath=" + this.basePath
					+ " maxFileSize in KB =" + this.maxFileSizeInKB + " sanitizingPattern=" + this.sanitizingPattern
					+ " base64EncodingNeeded=" + this.base64EncodingNeeded);
		}
	}

	public ImageRetriever(final Object basePath, final Object maxFileSize, final Object base64EncodingNeeded) {
		this.basePath =  (String) (isEmpty(basePath) ? "": basePath);
		this.maxFileSizeInKB = isEmpty(maxFileSize) ? DEFAULT_MAX_FILESIZE_KB : Integer.parseInt((String) maxFileSize);
		this.base64EncodingNeeded = base64EncodingNeeded == null ? false
				: Boolean.parseBoolean(base64EncodingNeeded.toString());

		//FIXME- try using the ESAPI validator approach instead for sanitizing...
		this.sanitizingPattern = DEFAULT_SANITIZED_PATTERN;
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " constructor " + " basePath=" + this.basePath
					+ " maxFileSize in KB =" + this.maxFileSizeInKB + " sanitizingPattern=" + this.sanitizingPattern
					+ " base64EncodingNeeded=" + this.base64EncodingNeeded);
		}
	}

	public Object getImage(final Object imagePath) {
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " getImage imagePath=" + imagePath);
		}
		Object img = null;
		if (isEmpty(imagePath)) {
			return img;
		}

		if (imagePath instanceof String) {
			img = getImageFromPath(basePath, (String) imagePath);
		} else if (imagePath instanceof byte[]) {
			img = imagePath;
		} else {
			logger.warn(CLASS_NAME+ " getImage imagePath TYPE is not defined...");
		}
		
		if (isBase64EncodingNeeded()) {
			if (logger.isTraceEnabled()) {
				logger.trace(CLASS_NAME + " getImage converting to base64 image");
			}
			img = getBase64EncodedImage((byte[])img);
		}
		return img;
	}

	private byte[] getImageFromPath(final String basePath, final String imagePath) {
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " getImageFromPath basePath=" + basePath+ " imagePath=" + imagePath);
		}
		if (isEmpty(imagePath)) {
			throw new IllegalArgumentException("ERROR  - image path cannot be null");
		}
		byte[] pixels = null;
		File imgFile = null;
		try {

			imgFile = new File(getSanitizedPath(basePath, imagePath, sanitizingPattern));
			if (isInvalid(imgFile)) {
				logger.warn(CLASS_NAME+ " getImageFromPath validation failed ... ");
				return pixels;
			}
			pixels = read(imgFile);
		} catch (IOException e) {
			logger.error(CLASS_NAME + " getImageFromPath ERROR " + e);
		} finally {
			// close(bufferedImgReader);
		}
		return pixels;
	}

	private byte[] read(final File imgFile) throws IOException {
		if (imgFile == null) {
			throw new IllegalArgumentException(CLASS_NAME
					+ " read ERROR  - imgFile cannot be null");
		}
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " read file size =" + imgFile.length());
		}
		//ImageIO.setUseCache(false);
		ByteArrayOutputStream os= null;
		BufferedImage bufferedImg = null;
		byte[] pixels = null;
		try{
			bufferedImg = ImageIO.read(imgFile);
			if(bufferedImg == null){
				logger.warn(CLASS_NAME + " read bufferedImg is null");
				return null;
			}

			if (logger.isTraceEnabled()) {
				logger.trace(CLASS_NAME + " read bufferedImg =" + bufferedImg.toString());
			}
			//pixels = ((DataBufferByte) bufferedImg.getRaster().getDataBuffer()).getData();

			os= new ByteArrayOutputStream();
			ImageIO.write(bufferedImg, getFormatName(imgFile), os);
			pixels= os.toByteArray();

		}finally{
			close(os);
		}

		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " read pixels length =" + pixels.length);
		}
		return pixels;
	}

	private static String getFormatName(final File imgFile) {
		if(imgFile==null || isEmpty(imgFile.getName())){
			return "";
		}
		String formatName = FilenameUtils.getExtension(imgFile.getName());
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " getFormatName formatName =" + formatName);
		}
		return formatName;
	}

	private boolean isInvalid(final File imgFile) {
		if (imgFile == null) {
			logger.warn(CLASS_NAME
					+ " getImageFromPath validation failed NULL imgFile="
					+ imgFile);
			return true;
		}
		if (!imgFile.exists()) {
			logger.warn(CLASS_NAME
					+ " getImageFromPath validation failed - image file does not exist! "
					+ imgFile.getAbsolutePath());
			return true;
		}
		if (imgFile.isDirectory()) {
			logger.warn(CLASS_NAME
					+ " getImageFromPath validation failed - path is a directory! "
					+ imgFile.getAbsolutePath());
			return true;
		}
		if (!imgFile.canRead()) {
			logger.warn(CLASS_NAME
					+ " getImageFromPath validation failed - cannot read- check permissions "
					+ imgFile.getAbsolutePath());
			return true;
		}
		if (imgFile.length() > maxFileSizeInKB * FileUtils.ONE_KB) {
			logger.warn(CLASS_NAME
					+ " getImageFromPath validation failed - Size is larger than default "
					+ imgFile.length());
			return true;
		}
		return false;
	}

	/**
	 *
	 * Performs the following
	 * a) concat of basepath and imagepath
	 * b) normalization of concat path
	 * c) sanitizing based on input regex pattern
	 *
	 * NOTE:
	 * No symlink check is being done
	 * No canonicalization check is being done	 *
	 * Obtaining regex and validating it with ESAPI is preferred than this approach
	 *
	 * @param basePath
	 * @param imagePath
	 * @param sanitizingPattern
	 * @return sanitizedFilePath
	 */
	private static String getSanitizedPath(final String basePath, final String imagePath,final String sanitizingPattern) {
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME+ " getSanitizedPath() basePath = "+ basePath +" imagePath="+imagePath +" sanitizingPattern="+sanitizingPattern);
		}
		String sanitizedFilePath ="";
		if (isEmpty(imagePath)) {
			//throw new IllegalArgumentException(CLASS_NAME+ " sanitize ERROR  - image path cannot be null");
			return sanitizedFilePath;
		}


		String concatFilePath = FilenameUtils.concat(basePath,imagePath);
		if(isEmpty(concatFilePath)){
			logger.warn(CLASS_NAME+ " getSanitizedPath() FAILED concat - check path...");
			return sanitizedFilePath;
		}
		String normalizedPath = FilenameUtils.normalize(concatFilePath.trim());
		if(isEmpty(normalizedPath)){
			logger.warn(CLASS_NAME+ " getSanitizedPath() FAILED normalization - check path...");
			return sanitizedFilePath;
		}
		sanitizedFilePath = normalizedPath.replaceAll(sanitizingPattern, "");
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME+ " getSanitizedPath() sanitizedFilePath = "+ sanitizedFilePath);
		}
		return sanitizedFilePath;
	}

	public static String getSanitizedPath(final String basePath, final String imagePath) {
		return getSanitizedPath(basePath, imagePath, DEFAULT_SANITIZED_PATTERN);
	}

	/**
	 *
	 * @param imageObj
	 * @return imageType
	 * @throws IOException
	 */
	public static String getImageType(final byte[] imageObj){
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME +  " getImageType() start");
		}
		String imageType = DEFAULT_JPG;
		if (imageObj == null) {
			return imageType;
		}
		ImageInputStream iis = null;
		ImageReader imageReader = null;
		try {
			iis = createImageInputStream(new ByteArrayInputStream(imageObj));
			Iterator<ImageReader> itt = getImageReaders(iis);
			while (itt.hasNext()) {
				imageReader = itt.next();
				imageType = imageReader.getFormatName();
				if (logger.isTraceEnabled()) {
					logger.trace(CLASS_NAME+ "getImageType() imageType decoded from reader =" + imageType);
				}
				if (!isEmpty(imageType)) {
					break;
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + "getImageType()", e);
		} finally {
			close(imageReader);
			close(iis);
		}
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + "getImageType() imageType="+ imageType);
		}
		return imageType;
	}
	
	public static Object getBase64EncodedImage(final byte[] img) {
		Object encodedImg = null;
		if(img!=null){
			encodedImg = new String(Base64.encodeBase64(img));
		}
		return encodedImg;
	}

	private static void close(final ImageReader imageReader) {
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " close() closing imageReader");
		}
		if (imageReader == null) {
			return;
		}
		try {
			imageReader.dispose();
		} catch (Exception e) {
			logger.error(CLASS_NAME + " close() ", e);
		}
	}

	private static void close(final ByteArrayOutputStream baos) {
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " close() closing ByteArrayOutputStream");
		}
		if (baos == null) {
			return;
		}
		try {
			baos.close();
		} catch (Exception e) {
			logger.error(CLASS_NAME + " close() ", e);
		}
	}


	private static void close(final ImageInputStream is) {
		if (logger.isTraceEnabled()) {
			logger.trace(CLASS_NAME + " close() closing ImageInputStream");
		}
		if (is == null) {
			return;
		}
		try {
			is.close();
		} catch (IOException e) {
			logger.error(CLASS_NAME + " close() ", e);
		}

	}

	public boolean isBase64EncodingNeeded() {
		return base64EncodingNeeded;
	}

	public void setBase64EncodingNeeded(final boolean base64EncodingNeeded) {
		this.base64EncodingNeeded = base64EncodingNeeded;
	}
}
