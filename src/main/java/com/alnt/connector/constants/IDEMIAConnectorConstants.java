package com.alnt.connector.constants;

public class IDEMIAConnectorConstants {
	
	public static final String CONNECTOR_NAME = "IDEMIAConnector";
	public static final String IDEMIA_USERID = "userId";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	
	public static final String ALERT_APP_DATE_FORMAT = "alertAppDateFormat";
	public static final String SHOW_PROVISION_WARNINGS = "showProvisioningWarnings";
	public static final String SENSITIVE_ATTRIBUTES = "sensitiveAttributes";
	public static final String CUSTOM_ATTRIBUTES = "customAttributes";

	public static final String CONN_PARAM_CONNECTION_TYPE = "connectionType";
	public static final String CONN_PARAM_SFTP_HOST = "ftphostname";
	public static final String CONN_PARAM_SFTP_USERNAME = "ftpusername";
	public static final String CONN_PARAM_SFTP_PASSWORD = "ftppassword";
	public static final String CONN_PARAM_SFTP_PORT = "ftpportnumber";
	public static final String CONN_PARAM_SFTP_REQUEST_FILE_LOCATION = "ftpRequestFileLocation";
	public static final String CONN_PARAM_SFTP_RESPONSE_FILE_LOCATION = "ftpResponseFileLocation";
	public static final String CONN_PARAM_IS_SSH = "isSSHAuthentication";
	public static final String CONN_PARAM_PRIVATE_KEY_PATH = "privateKeyFilePath";
	public static final String CONN_PARAM_PASSPHRASE = "passphrase";
	public static final String CONN_PARAM_TEMP_FILE_LOCATION = "tempFileLocation";
	public static final String CONN_PARAM_DATE_FIELDS_MAP = "dateFieldsMap";
	public static final String CONN_PARAM_PERSIST_BADGE_DETAILS = "PERSIST_BADGE_DETAILS";
	public static final String CONN_PARAM_USE_REQ_ID_AS_USER = "UseReqNumberasUserId";
	
	
	
	public static final String ATTR_TYPE_STRING = "String";
	public static final String ATTR_TYPE_INT = "Int";
	public static final String ATTR_TYPE_BOOLEAN = "Boolean";
	public static final String ATTR_TYPE_DATE = "Date";
	
	//Default values if connector parameter is not passed
	public static final String DEFAULT_DELIMITER = "|";
	public static final String CONCAT_DELIMITER = ";";
	public static final String HASH_DELIMITER = "#";
	public static final String AND_DELIMITER = "&";
	public static final String DEFAULT_CONNECTION_TYPE = "SFTP";
	public static final int DEFAULT_PORT = 22;
	
	public static final String SFTP = "SFTP";
	public static final String FTP = "FTP";
	
	
	
	// Attributes
	public static final String  RECORD_NUMBER="RecordNumber";
	public static final String  RECORD_SET_ID="RecordSetID";
	public static final String  RECORD_TYPE="RecordType";
	public static final String  PICTURE="Picture";
	public static final String  LAST_NAME="LastName";
	public static final String  FIRST_NAME="FirstName";
	public static final String  SIGNATURE_FIELD="SignatureField";
	public static final String  AGENCY_SPECIFIC_TEXT="AgencySpecificText";
	public static final String  RANK="Rank";
	public static final String  PDF417_2D_BARCODE="PDF4172DBarcode";
	public static final String  EMPLOYEE_AFFILIATION="EmployeeAffiliation";
	public static final String  HEADER="Header";
	public static final String  AGENCY_LINE1="AgencyLine1";
	public static final String  AGENCY_LINE2="AgencyLine2";
	public static final String  SEAL="Seal";
	public static final String  FOOTER="Footer";
	public static final String  ISSUE_DATE="IssueDate";
	public static final String  EXPIRATION_DATE="ExpirationDate";
	public static final String  COLOR_BAR_CODING="ColorBarcoding";
	public static final String  COLOR_BORDER_CODING="ColorBordercoding";
	public static final String  AGENCY_SPECIFIC_FRONT_TEXT1="AgencySpecificFrontText1";
	public static final String  AGENCY_SPECIFIC_FRONT_TEXT2="AgencySpecificFrontText2";
	public static final String  AGENCY_SPECIFIC_FRONT_TEXT3="AgencySpecificFrontText3";
	public static final String  AFFILIATION_COLOR_CODE="AffiliationColorCode";
	public static final String  EXPIRATION_DATE_FIPS_201_2="FIPSExpirationDate";
	public static final String  ORGANIZATIONAL_AFFILIATION="OrganizationalAffiliation";
	public static final String  AGENCY_CARD_SERIAL="AgencyCardSerial";
	public static final String  ISSUER_IDENTIFICATION="IssuerIdentification";
	public static final String  MAG_STRIPE_TRACK1="Mag_StripeTrack1";
	public static final String  MAG_STRIPE_TRACK2="Mag_StripeTrack2";
	public static final String  MAG_STRIPE_TRACK3="Mag_StripeTrack3";
	public static final String  RETURN_ADDRESS_LINE1="ReturnAddressline1";
	public static final String  RETURN_ADDRESS_LINE2="ReturnAddressline2";
	public static final String  RETURN_ADDRESS_LINE3="ReturnAddressline3";
	public static final String  HEIGHT="Height";
	public static final String  EYES="Eyes";
	public static final String  HAIR="Hair";
	public static final String  THREE_OF_9_BAR_CODE="3of9BarCode";
	public static final String  AGENCY_SPECIFIC_BACK_TEXT1="AgencySpecificBacktext1";
	public static final String  AGENCY_SPECIFIC_BACK_TEXT2="AgencySpecificBacktext2";
	public static final String  HEADER_INFORMATION="HeaderInformation";
	public static final String  RETURN_FILE_CREATION="ReturnFileCreation";
	public static final String  CLIENT_DEFINED_HEADER="ClientDefinedHeader";
	public static final String  AGENCY_CARD_SERIAL_NUMBER="AgencyCardSerialNumber";
	public static final String  BADGE_ID="BadgeId";
	public static final String  ISSUER_IDENTIFICATION_NUMBER="IssuerIdentificationNumber";
	public static final String  CPLC="CPLC";
	public static final String  STATUS="Status";
	public static final String  SHIPPING_METHOD="ShippingMethod";
	public static final String  SHIPPING_RECIPIENT_NAME="ShippingRecipientName";
	public static final String  SHIPPING_PHONE_NUMBER="ShippingPhoneNumber";
	public static final String  SHIPPING_STREET1="ShippingStreet1";
	public static final String  SHIPPING_STREET2="ShippingStreet2";
	public static final String  SHIPPING_CITY="ShippingCity";
	public static final String  SHIPPING_STATE="ShippingState";
	public static final String  SHIPPING_ZIP="ShippingZip";
	public static final String  SHIPPING_ZIP_SFX="ShippingZipSfx";
	public static final String  TRACKING_NUMBER="TrackingNumber";
	public static final String  SHIPPING_DATE="ShipDate";
	public static final String  BATCHUID="BatchUID";
	public static final String  AGENCY_HEADER_LABEL="AgencyHeaderLabel";
	public static final String  FILE_DATE="FileDate";
	public static final String  PHONE_NUMBER="PhoneNumber";
	public static final String  CARD_REQUEST_COUNT="CardRequestCount";
	public static final String  RECORD_LABEL="RecordLabel";
	public static final String  ALTERNATE_SHIPPING_RECIPIENT_NAME="AlternateShippingRecipientName";
	public static final String  ALTERNATE_PHONE_NUMBER="AlternatePhoneNumber";
	public static final String  ALTERNATE_SHIPPING_STREET1="AlternateShippingStreet1";
	public static final String  ALTERNATE_SHIPPING_STREET2="AlternateShippingStreet2";
	public static final String  ALTERNATE_SHIPPING_CITY="AlternateShippingCity";
	public static final String  ALTERNATE_SHIPPING_STATE="AlternateShippingState";
	public static final String  ALTERNATE_SHIPPING_ZIP="AlternateShippingZip";
	public static final String  ALTERNATE_SHIPPING_ZIP_SFX="AlternateShippingZipSfx";
	public static final String  SHIPPING_MODE="ShippingMode";
	public static final String  SHIPPING_TYPE="ShippingType";
	public static final String  CARDTYPE="CardType";
	public static final String  SLA_TURNAROUND_TIME="SLATurnaroundtime";
	public static final String  USER_IMAGE="UserImage";
	public static final String  LAST_NAME_FONT="LastNameFont";
	public static final String  FIRST_NAME_FONT="FirstNameFont";
	public static final String  FIRST_NAME_THIRD_LINE_FONT="FirstNameThirdlineFont";
	public static final String  FIRST_NAME_THIRD_LINE="FirstNameThirdline";
	public static final String  STATUS_CODE="StatusCode";
	public static final String  FAILUTE_DATE="FailureDate";
	
	
	// constants
	public static final String  CARD_FRONT_CODE="68A1";
	public static final String  CARD_BACK_CODE="68A2";
	public static final String  SHIPPING_RECORD="SHIPPING RECORD";
	public static final String  PICTURE_EXTN=".JPG";
	public static final String  ZIP_EXTN=".zip";
	public static final String  IMAGE_TYPE="jpg";
	public static final String  RESPONSE_FILE_SPLIT_STRING="\\|";
	
	
	public static final String  STATUS_FAILED="FAILED";
	public static final String  STATUS_SUCCESS="SUCCESS";
	public static final String  STATUS_WAITING="WAITING";
	
	
	public static final String  RESPONSE_FILE_SHIP="SHIP";
	public static final String  RESPONSE_FILE_RJCT="RJCT";
	public static final String  FOUND_USER_CONST="FOUNDUSER";
	public static final String  FOUND_USER_YES="YES";
	
	public static final int USERIMAGE_FILENAME_MAX_LENGTH = 23;
	public static final int USERIMAGE_FILENAME_EXTN_LENGTH = 4;
	
	public static final String REQ_NUMBER_ATTRIBUTE = "REQ_NO";
	
}
