package com.alnt.connector.exception;

public class IDEMIAConnectorException extends Exception{

	private static final long serialVersionUID = 41078271233392490L;

	public IDEMIAConnectorException(String message) {
		super(message);
	}
}
