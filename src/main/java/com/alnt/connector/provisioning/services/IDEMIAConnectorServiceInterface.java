package com.alnt.connector.provisioning.services;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningAttributes;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.access.provisioning.model.IProvisioningStatus;
import com.alnt.access.provisioning.model.ISystemInformation;
import com.alnt.access.provisioning.model.ProvisioningAttributes;
import com.alnt.access.provisioning.model.ProvisioningStatus;
import com.alnt.access.provisioning.services.IConnectionInterface;
import com.alnt.access.provisioning.utils.ConnectorUtil;
import com.alnt.connector.constants.IDEMIAConnectorConstants;
import com.alnt.connector.exception.IDEMIAConnectorException;
import com.alnt.connector.helper.IDEMIAClientHelper;
import com.alnt.connector.provisioning.model.ProvisioningResult;
import com.alnt.extractionconnector.common.model.IRoleInformation;
import com.alnt.extractionconnector.common.model.IUserInformation;
import com.alnt.extractionconnector.common.service.IExtractionInterface;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.exception.ExtractorConnectionException;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;

public class IDEMIAConnectorServiceInterface implements IExtractionInterface, IConnectionInterface {

	private static final String CLASS_NAME = "com.alnt.connector.provisioning.services.IDEMIAConnectorServiceInterface";
	private static Logger logger = Logger.getLogger(CLASS_NAME);
	// common across all connectors
	private String _externalUserIdAttribute = null;
	private String _alertAppDateFormat = null;
	@SuppressWarnings("unused")
	private boolean _showProvisioningWarnings = true;
	protected Set<String> excludeLogAttrList = null;
	protected Set<String> customAttrList = null;

	// Connector Specific

	private String _delimiter = IDEMIAConnectorConstants.DEFAULT_DELIMITER;
	private String _concatDelimiter = IDEMIAConnectorConstants.CONCAT_DELIMITER;
	private String _connectionType = IDEMIAConnectorConstants.DEFAULT_CONNECTION_TYPE;
	private int _ftpportnumber = IDEMIAConnectorConstants.DEFAULT_PORT;
	private String _tempFileLocation = null;
	private boolean _isSSHAuthentication = false;
	@SuppressWarnings("unused")
	private boolean _persistBadgeDetails = false;
	private String _ftphostname = null;
	private String _ftpusername = null;
	private String _ftppassword = null;
	private String _ftpRequestFileLocation = "/";
	private String _ftpResponseFileLocation = "/";
	private String _privateKeyFilePath = null;
	private String _passphrase = null;
	private int record = 0;
	private String recordSetId = "00000000";
	private String _badgeType = "";
	private boolean  _useReqIdAsUserId = true;
	private Map<String, String> dateFieldsMap = new LinkedHashMap<String, String>();

	public IDEMIAConnectorServiceInterface(Map<String, String> connectionParams) throws IDEMIAConnectorException {
		try {

			_alertAppDateFormat = (String) connectionParams.get(IDEMIAConnectorConstants.ALERT_APP_DATE_FORMAT);

			if (connectionParams.containsKey(IDEMIAConnectorConstants.SHOW_PROVISION_WARNINGS)) {
				_showProvisioningWarnings = Boolean.parseBoolean(connectionParams.get(IDEMIAConnectorConstants.SHOW_PROVISION_WARNINGS));
			}
			_externalUserIdAttribute = IDEMIAConnectorConstants.IDEMIA_USERID;
			String excludeAttributes = connectionParams.get(IDEMIAConnectorConstants.SENSITIVE_ATTRIBUTES);
			if (excludeAttributes != null && !excludeAttributes.isEmpty()) {
				excludeLogAttrList = ConnectorUtil.convertStringToList(excludeAttributes, ",");
			}
			if (excludeLogAttrList == null) {
				excludeLogAttrList = new HashSet<String>();
			}
			String customAttributes = connectionParams.get(IDEMIAConnectorConstants.CUSTOM_ATTRIBUTES);
			if (customAttributes != null && !customAttributes.isEmpty()) {
				customAttrList = ConnectorUtil.convertStringToList(customAttributes, ",");
			}
			if (customAttrList == null) {
				customAttrList = new HashSet<String>();
			}
			_delimiter = IDEMIAConnectorConstants.DEFAULT_DELIMITER;
			_connectionType = IDEMIAConnectorConstants.DEFAULT_CONNECTION_TYPE;
			_ftpportnumber = IDEMIAConnectorConstants.DEFAULT_PORT;

			if (connectionParams.containsKey(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE)) {
				_connectionType = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_CONNECTION_TYPE);
			}
			// By default requestId used as UserId
			if (connectionParams.containsKey(IDEMIAConnectorConstants.CONN_PARAM_USE_REQ_ID_AS_USER)) {
				_useReqIdAsUserId = Boolean.parseBoolean(connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_USE_REQ_ID_AS_USER));
			}
			
			
			_ftphostname = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_SFTP_HOST);
			_ftpusername = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_SFTP_USERNAME);
			_ftppassword = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PASSWORD);
			if (connectionParams.containsKey(IDEMIAConnectorConstants.CONN_PARAM_PERSIST_BADGE_DETAILS)) {
				_persistBadgeDetails = Boolean.parseBoolean(connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_PERSIST_BADGE_DETAILS));
			}

			_ftpRequestFileLocation = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_SFTP_REQUEST_FILE_LOCATION);
			_ftpResponseFileLocation = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_SFTP_RESPONSE_FILE_LOCATION);
			_tempFileLocation = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_TEMP_FILE_LOCATION);
			if (connectionParams.containsKey(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT)) {
				_ftpportnumber = Integer.parseInt(connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_SFTP_PORT));
			}
			if (connectionParams.containsKey(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH)) {
				_isSSHAuthentication = Boolean.parseBoolean(connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_IS_SSH));
			}
			if (connectionParams.containsKey(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH)) {
				_privateKeyFilePath = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_PRIVATE_KEY_PATH);
			}
			if (connectionParams.containsKey(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE)) {
				_passphrase = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_PASSPHRASE);
			}
			
			if (connectionParams.containsKey(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP)) {
				if (null != connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP) && !(connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP).isEmpty())) {
					String dateString = connectionParams.get(IDEMIAConnectorConstants.CONN_PARAM_DATE_FIELDS_MAP);
					StringTokenizer st = new StringTokenizer(dateString, IDEMIAConnectorConstants.DEFAULT_DELIMITER);
					while (st.hasMoreTokens()) {
						String field = st.nextToken().toString();
						if (null != field && !field.isEmpty()) {
							String[] pairs = field.split(IDEMIAConnectorConstants.CONCAT_DELIMITER);
							if (null != pairs && pairs.length >= 2)
								dateFieldsMap.put(pairs[0], pairs[1]);
						}
					}
				}
			}
		} catch (Exception _e) {
			throw new IDEMIAConnectorException("connection parameters should be not be NULL");
		}

	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getAllRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<IRoleInformation> getAllRoles(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void getAllRoles(Map<String, List<ExtractorAttributes>> arg0, int arg1, Map<String, String> arg2, ISearchCallback arg3) throws ExtractorConnectionException {
	}

	@Override
	public List<IRoleInformation> getIncrementalRoles(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void getIncrementalRoles(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, Map<String, String> arg3, ISearchCallback arg4) throws ExtractorConnectionException {
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getRolesForUser(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void searchRoles(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {

	}

	@Override
	public boolean supportsProvisioning() {

		return false;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Map getAllUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<IUserInformation> getAllUsers(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {

		return null;
	}

	@Override
	public void getAllUsersWithCallback(Map<String, List<ExtractorAttributes>> arg0, int arg1, Map<String, String> arg2, ISearchCallback arg3) throws ExtractorConnectionException {

	}

	@Override
	public List<IUserInformation> getIncrementalUsers(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void getIncrementalUsersWithCallback(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, Map<String, String> arg3, ISearchCallback arg4) throws ExtractorConnectionException {

	}

	@Override
	@SuppressWarnings("rawtypes")
	public Map getUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void getUsers(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {

	}

	@Override
	@SuppressWarnings("rawtypes")
	public void invokeUsers(Map arg0, List arg1) throws ExtractorConnectionException {

	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult activateBadge(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {

		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult addBadge(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {

		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult addTempBadge(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult changeAccess(Long arg0, List arg1, Map arg2, List arg3, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg4, Map<String, String> arg5) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult changeBadge(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult changeBadgeRoles(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	private IProvisioningResult prepareProvisioningResult(String provisioningAction, String msgCode, Boolean isFailed, String msgDesc) {
		IProvisioningResult provisioningResult = new ProvisioningResult();
		provisioningResult.setMsgCode(msgCode);
		if (null != isFailed) {
			provisioningResult.setProvFailed(isFailed);
		}
		provisioningResult.setMsgDesc(msgDesc);
		provisioningResult.setProvAction(provisioningAction);
		return provisioningResult;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public IProvisioningResult create(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " create(): Start of Create Method.");
		IProvisioningResult provResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "User is not provisioned !!!");
		boolean userCreated = true;
		try {
			ConnectorUtil.logInputParamters(parameters, excludeLogAttrList, logger);
			String userId = "00000000";
			if (parameters.get(IDEMIAConnectorConstants.RECORD_SET_ID) != null && !parameters.get(IDEMIAConnectorConstants.RECORD_SET_ID).toString().isEmpty()) {
				// as per document it can be alphanumeric , so not using format of number
				userId = parameters.get(IDEMIAConnectorConstants.RECORD_SET_ID).toString();
			}
			if(this._useReqIdAsUserId) {
				recordSetId = requestNumber.toString();
			}else {
				recordSetId=userId;
			}
			
			if (parameters.get(IDEMIAConnectorConstants.CARDTYPE) != null && !parameters.get(IDEMIAConnectorConstants.CARDTYPE).toString().isEmpty()) {
				_badgeType = parameters.get(IDEMIAConnectorConstants.CARDTYPE).toString();
			}
		
			
			provisionToFile(parameters);

			provResult.setUserId(userId);
			provResult.setUserCreated(userCreated);
			provResult.setMsgCode(IProvisoiningConstants.CREATE_USER_SUCCESS);
			provResult.setMsgDesc("User provisioned successfully");
			provResult.setProvFailed(false);
			parameters.put(_externalUserIdAttribute, userId);

		} catch (Exception e) {
			logger.error(CLASS_NAME + "create()", e);
			provResult.setMsgDesc(e.getMessage());
			return provResult;
		}
		logger.debug(CLASS_NAME + " create(): End of Create Method.");
		return provResult;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult create(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4, Map<String, String> arg5) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult deActivateBadge(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult deleteAccount(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult delimitUser(Long arg0, List arg1, Map arg2, List arg3, String arg4, Map<String, String> arg5) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getAttributes() throws Exception {
		List<IProvisioningAttributes> provAttributes = new ArrayList<IProvisioningAttributes>();
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RECORD_NUMBER, IDEMIAConnectorConstants.RECORD_NUMBER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RECORD_SET_ID, IDEMIAConnectorConstants.RECORD_SET_ID, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RECORD_TYPE, IDEMIAConnectorConstants.RECORD_TYPE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.PICTURE, IDEMIAConnectorConstants.PICTURE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.LAST_NAME, IDEMIAConnectorConstants.LAST_NAME, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.FIRST_NAME, IDEMIAConnectorConstants.FIRST_NAME, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SIGNATURE_FIELD, IDEMIAConnectorConstants.SIGNATURE_FIELD, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_SPECIFIC_TEXT, IDEMIAConnectorConstants.AGENCY_SPECIFIC_TEXT, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RANK, IDEMIAConnectorConstants.RANK, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.PDF417_2D_BARCODE, IDEMIAConnectorConstants.PDF417_2D_BARCODE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.EMPLOYEE_AFFILIATION, IDEMIAConnectorConstants.EMPLOYEE_AFFILIATION, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.HEADER, IDEMIAConnectorConstants.HEADER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_LINE1, IDEMIAConnectorConstants.AGENCY_LINE1, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_LINE2, IDEMIAConnectorConstants.AGENCY_LINE2, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SEAL, IDEMIAConnectorConstants.SEAL, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.FOOTER, IDEMIAConnectorConstants.FOOTER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ISSUE_DATE, IDEMIAConnectorConstants.ISSUE_DATE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.EXPIRATION_DATE, IDEMIAConnectorConstants.EXPIRATION_DATE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.COLOR_BAR_CODING, IDEMIAConnectorConstants.COLOR_BAR_CODING, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.COLOR_BORDER_CODING, IDEMIAConnectorConstants.COLOR_BORDER_CODING, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT1, IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT1, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT2, IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT2, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT3, IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT3, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AFFILIATION_COLOR_CODE, IDEMIAConnectorConstants.AFFILIATION_COLOR_CODE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.EXPIRATION_DATE_FIPS_201_2, IDEMIAConnectorConstants.EXPIRATION_DATE_FIPS_201_2, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ORGANIZATIONAL_AFFILIATION, IDEMIAConnectorConstants.ORGANIZATIONAL_AFFILIATION, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL, IDEMIAConnectorConstants.AGENCY_CARD_SERIAL, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION, IDEMIAConnectorConstants.ISSUER_IDENTIFICATION, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.MAG_STRIPE_TRACK1, IDEMIAConnectorConstants.MAG_STRIPE_TRACK1, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.MAG_STRIPE_TRACK2, IDEMIAConnectorConstants.MAG_STRIPE_TRACK2, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.MAG_STRIPE_TRACK3, IDEMIAConnectorConstants.MAG_STRIPE_TRACK3, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE1, IDEMIAConnectorConstants.RETURN_ADDRESS_LINE1, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE2, IDEMIAConnectorConstants.RETURN_ADDRESS_LINE2, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RETURN_ADDRESS_LINE3, IDEMIAConnectorConstants.RETURN_ADDRESS_LINE3, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.HEIGHT, IDEMIAConnectorConstants.HEIGHT, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.EYES, IDEMIAConnectorConstants.EYES, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.HAIR, IDEMIAConnectorConstants.HAIR, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.THREE_OF_9_BAR_CODE, IDEMIAConnectorConstants.THREE_OF_9_BAR_CODE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1, IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT2, IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT2, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.HEADER_INFORMATION, IDEMIAConnectorConstants.HEADER_INFORMATION, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RETURN_FILE_CREATION, IDEMIAConnectorConstants.RETURN_FILE_CREATION, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.CLIENT_DEFINED_HEADER, IDEMIAConnectorConstants.CLIENT_DEFINED_HEADER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL_NUMBER, IDEMIAConnectorConstants.AGENCY_CARD_SERIAL_NUMBER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION_NUMBER, IDEMIAConnectorConstants.ISSUER_IDENTIFICATION_NUMBER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.CPLC, IDEMIAConnectorConstants.CPLC, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.STATUS, IDEMIAConnectorConstants.STATUS, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_METHOD, IDEMIAConnectorConstants.SHIPPING_METHOD, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME, IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_PHONE_NUMBER, IDEMIAConnectorConstants.SHIPPING_PHONE_NUMBER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_STREET1, IDEMIAConnectorConstants.SHIPPING_STREET1, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_STREET2, IDEMIAConnectorConstants.SHIPPING_STREET2, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_CITY, IDEMIAConnectorConstants.SHIPPING_CITY, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_STATE, IDEMIAConnectorConstants.SHIPPING_STATE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_ZIP, IDEMIAConnectorConstants.SHIPPING_ZIP, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_ZIP_SFX, IDEMIAConnectorConstants.SHIPPING_ZIP_SFX, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.TRACKING_NUMBER, IDEMIAConnectorConstants.TRACKING_NUMBER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_DATE, IDEMIAConnectorConstants.SHIPPING_DATE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.BATCHUID, IDEMIAConnectorConstants.BATCHUID, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.AGENCY_HEADER_LABEL, IDEMIAConnectorConstants.AGENCY_HEADER_LABEL, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.FILE_DATE, IDEMIAConnectorConstants.FILE_DATE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.PHONE_NUMBER, IDEMIAConnectorConstants.PHONE_NUMBER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.CARD_REQUEST_COUNT, IDEMIAConnectorConstants.CARD_REQUEST_COUNT, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.RECORD_LABEL, IDEMIAConnectorConstants.RECORD_LABEL, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_RECIPIENT_NAME, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_RECIPIENT_NAME, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ALTERNATE_PHONE_NUMBER, IDEMIAConnectorConstants.ALTERNATE_PHONE_NUMBER, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET1, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET1, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET2, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET2, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_CITY, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_CITY, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STATE, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STATE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP_SFX, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP_SFX, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_MODE, IDEMIAConnectorConstants.SHIPPING_MODE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SHIPPING_TYPE, IDEMIAConnectorConstants.SHIPPING_TYPE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.CARDTYPE, IDEMIAConnectorConstants.CARDTYPE, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.SLA_TURNAROUND_TIME, IDEMIAConnectorConstants.SLA_TURNAROUND_TIME, false, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(IDEMIAConnectorConstants.USER_IMAGE, IDEMIAConnectorConstants.USER_IMAGE, true, IDEMIAConnectorConstants.ATTR_TYPE_STRING));
		return provAttributes;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getAttributes(Map arg0) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getBadgeLastLocation(String arg0) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getDetailsAsList(String arg0, String arg1) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List getExistingBadges(String arg0) throws Exception {
		return null;
	}

	private String createEmptyDirectoryForFTPRecon(String dirPath) {
		String methodName = "createEmptyDirectory";
		File theDir = new File(dirPath);
		if (!theDir.exists()) {
			boolean result = false;
			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				logger.error(CLASS_NAME + methodName + "Exception at creating ReconFTP Local Directory-->" + dirPath);
				return null;
			}
			if (result) {
				logger.debug(CLASS_NAME + methodName + "Recon through FTP. Local Directory Created-->" + dirPath);
			}
		}
		return dirPath;
	}

	@SuppressWarnings("unused")
	@Override
	public IProvisioningStatus getProvisioningStatus(Map<String, String> data) throws Exception {
		final String METHOD_NAME = "getProvisioningStatus()";
		IProvisioningStatus provStatus = new ProvisioningStatus();
		String userId = data.get(IDEMIAConnectorConstants.RECORD_SET_ID); // RecordSetID
		if (null != userId && !userId.isEmpty()) {
			String directoryToCopyFTPFiles = _tempFileLocation + userId + "_" + System.currentTimeMillis();
			createEmptyDirectoryForFTPRecon(directoryToCopyFTPFiles);
			boolean isSuccess = false;
			if (_connectionType.equalsIgnoreCase(IDEMIAConnectorConstants.SFTP)) {
				isSuccess = downloadFilesFromSFTP(_ftpResponseFileLocation, directoryToCopyFTPFiles);
				logger.debug(CLASS_NAME + METHOD_NAME + "downloded Files from Server to " + directoryToCopyFTPFiles);
			} else if (_connectionType.equalsIgnoreCase(IDEMIAConnectorConstants.FTP)) {
				isSuccess = downloadFilesFromFTP(_ftpResponseFileLocation, directoryToCopyFTPFiles);
				logger.debug(CLASS_NAME + METHOD_NAME + "downloded Files from Server to " + directoryToCopyFTPFiles);
			}
			File folder = new File(directoryToCopyFTPFiles);
			File[] fileNames = folder.listFiles();
			Boolean foundUser = false;
			if (null != fileNames && fileNames.length > 0) {
				for (File file : fileNames) {
					if (!foundUser) {
						String fileName = file.getName();
						logger.debug(CLASS_NAME + METHOD_NAME + "fileName  ::  " + fileName);
						String fileType = fileName.substring(3, 7);
						if (fileType.equalsIgnoreCase(IDEMIAConnectorConstants.RESPONSE_FILE_SHIP)) {
							Map<String, String> successResponse = processShipmentFile(data, file.getPath());
							if (null != successResponse.get(IDEMIAConnectorConstants.FOUND_USER_CONST) && successResponse.get(IDEMIAConnectorConstants.FOUND_USER_CONST).equalsIgnoreCase(IDEMIAConnectorConstants.FOUND_USER_YES)) {
								foundUser = true;
								provStatus.setParameters(successResponse);
								break;
							}
						} else if (fileType.equalsIgnoreCase(IDEMIAConnectorConstants.RESPONSE_FILE_RJCT)) {
							Map<String, String> rejectedResponse = processRejectFile(data, file.getPath());
							if (null != rejectedResponse.get(IDEMIAConnectorConstants.FOUND_USER_CONST) && rejectedResponse.get(IDEMIAConnectorConstants.FOUND_USER_CONST).equalsIgnoreCase(IDEMIAConnectorConstants.FOUND_USER_YES)) {
								foundUser = true;
								provStatus.setParameters(rejectedResponse);
								break;
							}
						}
					}
				}
				if (!foundUser) {
					logger.debug(CLASS_NAME + METHOD_NAME + "IN THE EXISTING FILES THIS USER RECORD IS NOT FOUND");
					data.put(IDEMIAConnectorConstants.STATUS, IDEMIAConnectorConstants.STATUS_WAITING);
					provStatus.setParameters(data);
				}
			} else {
				logger.debug(CLASS_NAME + METHOD_NAME + "NO FILES FOUND AT RESPONSE LOCATION , SO STATUS IS WAITING");
				data.put(IDEMIAConnectorConstants.STATUS, IDEMIAConnectorConstants.STATUS_WAITING);
				provStatus.setParameters(data);
			}
		} else {
			logger.debug(CLASS_NAME + METHOD_NAME + "UserId is not passed in request map , please set RecordSetID key with UserId value");
			provStatus.setErrorCode("NO_USERID_IN_REQUEST");
			provStatus.setMessageDesc("UserId  is not passed in request map");
		}
		return provStatus;
	}

	Map<String, String> processRejectFile(Map<String, String> data, String fileLocation) {
		Map<String, String> response = null;
		BufferedReader reader;
		try {
			int rowNumber = 1;
			reader = new BufferedReader(new FileReader(fileLocation));
			String line = reader.readLine();
			if (rowNumber == 1 && null != line) {
				processResponseHeader(line, data);
			}
			while (line != null) {
				if (rowNumber > 1) {
					response = processRejectRecord(line, data);
					if (null != response.get(IDEMIAConnectorConstants.FOUND_USER_CONST) && response.get(IDEMIAConnectorConstants.FOUND_USER_CONST).equalsIgnoreCase(IDEMIAConnectorConstants.FOUND_USER_YES)) {
						break;
					}
				}
				rowNumber++;
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;

	}

	Map<String, String> processShipmentFile(Map<String, String> data, String fileLocation) {
		Map<String, String> response = null;
		BufferedReader reader;
		try {
			int rowNumber = 1;
			reader = new BufferedReader(new FileReader(fileLocation));
			String line = reader.readLine();
			if (rowNumber == 1 && null != line) {
				processResponseHeader(line, data);
			}
			while (line != null) {
				if (rowNumber > 1) {
					response = processShipmentRecord(line, data);
					if (null != response.get(IDEMIAConnectorConstants.FOUND_USER_CONST) && response.get(IDEMIAConnectorConstants.FOUND_USER_CONST).equalsIgnoreCase(IDEMIAConnectorConstants.FOUND_USER_YES)) {
						break;
					}
				}
				rowNumber++;
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;

	}

	private Map<String, String> processShipmentRecord(String recordString, Map<String, String> data) {
		Map<String, String> shipmentRecordMap = new HashMap<String, String>();
		String[] pairs = recordString.split(IDEMIAConnectorConstants.RESPONSE_FILE_SPLIT_STRING, -1);
		String userId = data.get(IDEMIAConnectorConstants.RECORD_SET_ID);
		if (pairs[1].equalsIgnoreCase(userId) && pairs.length == 18) {
			shipmentRecordMap.put(IDEMIAConnectorConstants.RECORD_NUMBER, pairs[0]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.RECORD_SET_ID, pairs[1]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL_NUMBER, pairs[2]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.BADGE_ID, pairs[2]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION_NUMBER, pairs[3]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.CPLC, pairs[4]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.STATUS, IDEMIAConnectorConstants.STATUS_SUCCESS);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_METHOD, pairs[6]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME, pairs[7]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_PHONE_NUMBER, pairs[8]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_STREET1, pairs[9]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_STREET2, pairs[10]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_CITY, pairs[11]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_STATE, pairs[12]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_ZIP, pairs[13]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_ZIP_SFX, pairs[14]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.TRACKING_NUMBER, pairs[15]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.SHIPPING_DATE, pairs[16]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.BATCHUID, pairs[17]);
			shipmentRecordMap.put(IDEMIAConnectorConstants.FOUND_USER_CONST, IDEMIAConnectorConstants.FOUND_USER_YES);
		}
		return shipmentRecordMap;
	}

	private Map<String, String> processRejectRecord(String recordString, Map<String, String> data) {
		Map<String, String> rejectRecordMap = new HashMap<String, String>();
		String userId = data.get(IDEMIAConnectorConstants.RECORD_SET_ID);
		String[] pairs = recordString.split(IDEMIAConnectorConstants.RESPONSE_FILE_SPLIT_STRING, -1);
		if (pairs[1].equalsIgnoreCase(userId) && pairs.length == 8) {
			rejectRecordMap.put(IDEMIAConnectorConstants.RECORD_NUMBER, pairs[0]);
			rejectRecordMap.put(IDEMIAConnectorConstants.RECORD_SET_ID, pairs[1]);
			rejectRecordMap.put(IDEMIAConnectorConstants.AGENCY_CARD_SERIAL_NUMBER, pairs[2]);
			rejectRecordMap.put(IDEMIAConnectorConstants.BADGE_ID, pairs[2]);
			rejectRecordMap.put(IDEMIAConnectorConstants.ISSUER_IDENTIFICATION_NUMBER, pairs[3]);
			rejectRecordMap.put(IDEMIAConnectorConstants.STATUS, IDEMIAConnectorConstants.STATUS_FAILED);
			rejectRecordMap.put(IDEMIAConnectorConstants.STATUS_CODE, pairs[5]);
			rejectRecordMap.put(IDEMIAConnectorConstants.FAILUTE_DATE, pairs[6]);
			rejectRecordMap.put(IDEMIAConnectorConstants.BATCHUID, pairs[7]);
			rejectRecordMap.put(IDEMIAConnectorConstants.FOUND_USER_CONST, IDEMIAConnectorConstants.FOUND_USER_YES);
		}
		return rejectRecordMap;
	}

	static void processResponseHeader(String headerString, Map<String, String> data) {
		Map<String, String> headerResponse = new HashMap<String, String>();
		String[] pairs = headerString.split(IDEMIAConnectorConstants.RESPONSE_FILE_SPLIT_STRING, -1);
		if (pairs.length == 4) {
			headerResponse.put(IDEMIAConnectorConstants.RECORD_NUMBER, pairs[0]);
			headerResponse.put(IDEMIAConnectorConstants.HEADER_INFORMATION, pairs[1]);
			headerResponse.put(IDEMIAConnectorConstants.RETURN_FILE_CREATION, pairs[2]);
			headerResponse.put(IDEMIAConnectorConstants.CLIENT_DEFINED_HEADER, pairs[3]);
		}
	}

	@Override
	public boolean isUserLocked(String arg0) throws Exception {
		return false;
	}

	@Override
	public ISystemInformation isUserProvisioned(String arg0) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult lock(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List provision(Long arg0, String arg1, List arg2, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg3, Map arg4, List arg5, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg6,
			Map<String, String> arg7) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult removeBadge(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	@Override
	public void setTaskId(Long arg0) {

	}

	@Override
	public boolean testConnection() throws Exception {
		final String METHOD_NAME = "testConnection()";
		if (_connectionType.equalsIgnoreCase(IDEMIAConnectorConstants.SFTP)) {
			Channel channel = IDEMIAClientHelper.getSFTPConnection(_ftphostname, _ftpusername, _ftppassword, _ftpportnumber, _privateKeyFilePath, _passphrase, _isSSHAuthentication);
			if (channel != null) {
				try {
					channel.getSession().disconnect();
					return true;
				} catch (JSchException e) {
				}
				logger.debug(CLASS_NAME + " " + METHOD_NAME + " makeSFTPConnection(): Returning value as true ");
				return true;
			} else {
				logger.debug(CLASS_NAME + " " + METHOD_NAME + " makeSFTPConnection(): Returning value as false ");
				return false;
			}
		}
		if (_connectionType.equalsIgnoreCase(IDEMIAConnectorConstants.FTP)) {
			FTPClient ftpClient = IDEMIAClientHelper.getFTPConnection(_ftphostname, _ftpusername, _ftppassword, _ftpportnumber, _isSSHAuthentication);
			if (ftpClient != null) {
				try {
					ftpClient.logout();
					ftpClient.disconnect();
					return true;
				} catch (Exception e) {
					logger.error(CLASS_NAME + " " + METHOD_NAME + " makeFTPConnection" + "error in closing FTP :", e);
				}
				return true;
			}
		}

		return false;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult unlock(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult update(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4) throws Exception {
		return null;
	}

	@Override
	public IProvisioningResult updatePassword(Long arg0, String arg1, String arg2, String arg3) throws Exception {
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	List<String> saveLocally(Map data) {
		final String METHOD_NAME = "saveLocally()";
		List<String> userFiles = new ArrayList<String>();
		logger.debug(CLASS_NAME + " " + METHOD_NAME + "Going to save User Image ");
		String userImagePath = saveUserImage(data);
		logger.debug(CLASS_NAME + " " + METHOD_NAME + " User Image  path after Saving  ::" + userImagePath);
		if (null != userImagePath) {
			userFiles.add(userImagePath);
		}
		logger.debug(CLASS_NAME + " " + METHOD_NAME + "Going to save User Data ");
		String RecodFile = saveRecordDataFile(data);
		logger.debug(CLASS_NAME + " " + METHOD_NAME + "Data file After processing data  :: " + RecodFile);
		userFiles.add(RecodFile);
		return userFiles;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String saveUserImage(Map data) {
		final String METHOD_NAME = "saveUserImage()";
		String picturePath = null;
		if (data.get(IDEMIAConnectorConstants.USER_IMAGE) != null && !data.get(IDEMIAConnectorConstants.USER_IMAGE).toString().isEmpty()) {
			logger.debug(CLASS_NAME + " " + METHOD_NAME + "User Image is not Empty ");
			byte[] dataArr = (byte[]) data.get(IDEMIAConnectorConstants.USER_IMAGE);
			ByteArrayInputStream bis = new ByteArrayInputStream(dataArr);
			BufferedImage bImage2;
			try {
				if (data.get(IDEMIAConnectorConstants.PICTURE) != null && !data.get(IDEMIAConnectorConstants.PICTURE).toString().isEmpty()) {
					logger.debug(CLASS_NAME + " " + METHOD_NAME + "User Image file in the request ");
					picturePath = _tempFileLocation + data.get(IDEMIAConnectorConstants.PICTURE).toString();
					logger.debug(CLASS_NAME + " " + METHOD_NAME + "User Image full path   " + picturePath);
				} else {
					logger.debug(CLASS_NAME + " " + METHOD_NAME + "User Image file name is not in the request , creating image name ");
					int stringlengthTOGenerate = IDEMIAConnectorConstants.USERIMAGE_FILENAME_MAX_LENGTH - (recordSetId.length() + _badgeType.length()+1 + IDEMIAConnectorConstants.USERIMAGE_FILENAME_EXTN_LENGTH);
					String generatedString = getAlphaNumericString(stringlengthTOGenerate);
					String filename = _badgeType+"_"+recordSetId + generatedString.toUpperCase() + IDEMIAConnectorConstants.PICTURE_EXTN;
					picturePath = _tempFileLocation + filename;
					logger.debug(CLASS_NAME + " " + METHOD_NAME + "User Image full path   " + picturePath);
					data.put(IDEMIAConnectorConstants.PICTURE, filename);
				}
				bImage2 = ImageIO.read(bis);
				ImageIO.write(bImage2, IDEMIAConnectorConstants.IMAGE_TYPE, new File(picturePath));
			} catch (IOException e) {
				logger.debug(CLASS_NAME + " " + METHOD_NAME + "Error While saving User Image   " + e.getMessage());
				e.printStackTrace();
			}
			return picturePath;
		} else {
			logger.debug(CLASS_NAME + " " + METHOD_NAME + "User Image is not passed / is Empty ");
		}
		return picturePath;
	}

	private String saveRecordDataFile(Map<String, String> data) {
		final String METHOD_NAME = "saveRecordDataFile()";
		String filename = _tempFileLocation + _badgeType+"_"+ System.currentTimeMillis() + "_" + ThreadLocalRandom.current().nextInt(0, 9999) + ".txt";
		logger.debug(CLASS_NAME + " " + METHOD_NAME + "User Record file full path   " + filename);
		try {
			FileOutputStream outputStream;
			outputStream = new FileOutputStream(filename);
			PrintStream fileps = new PrintStream(outputStream);
			genHeader(data, fileps);
			genAlternativeHeader(data, fileps);
			genFront(data, fileps);
			genBack(data, fileps);
			outputStream.close();
		} catch (IOException e) {
			logger.debug(CLASS_NAME + " " + METHOD_NAME + "Error While saving saveRecordDataFile   " + e.getMessage());
			return null;
		}
		return filename;
	}

	private void zipUserFiles(List<String> files, String fileName) throws FileNotFoundException {
		try {
			FileOutputStream fos;
			fos = new FileOutputStream(fileName);
			ZipOutputStream zipOut = new ZipOutputStream(fos);
			for (String srcFile : files) {
				File fileToZip = new File(srcFile);
				FileInputStream fis = new FileInputStream(fileToZip);
				ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
				zipOut.putNextEntry(zipEntry);

				byte[] bytes = new byte[1024];
				int length;
				while ((length = fis.read(bytes)) >= 0) {
					zipOut.write(bytes, 0, length);
				}
				fis.close();
			}
			zipOut.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes" })
	private void provisionToFile(Map data) throws Exception {
		final String METHOD_NAME = "provisionToFile()";
		List<String> filesToZip = saveLocally(data);
		String fileName = _tempFileLocation + recordSetId + "_" + System.currentTimeMillis() + "_" + ThreadLocalRandom.current().nextInt(0, 99999) + IDEMIAConnectorConstants.ZIP_EXTN;
		logger.debug(CLASS_NAME + " " + METHOD_NAME + " Local zip file location " + fileName);
		zipUserFiles(filesToZip, fileName);
		if (_connectionType.equalsIgnoreCase(IDEMIAConnectorConstants.SFTP)) {
			uploadFileToSFTP(fileName, _ftpRequestFileLocation);
		}
		if (_connectionType.equalsIgnoreCase(IDEMIAConnectorConstants.FTP)) {
			uploadFileToFTP(fileName, _ftpRequestFileLocation);
		}
	}

	public void uploadFileToSFTP(String filename, String destination) throws Exception {
		Channel sftpChannel = null;
		FileInputStream in = null;
		try {
			logger.info(CLASS_NAME + " uploadFileToSFTP(): Start of method");
			sftpChannel = IDEMIAClientHelper.getSFTPConnection(_ftphostname, _ftpusername, _ftppassword, _ftpportnumber, _privateKeyFilePath, _passphrase, _isSSHAuthentication);
			File filetoupload = new File(filename);
			ChannelSftp sfpChan = ((ChannelSftp) sftpChannel);
			sfpChan.cd(destination);
			in = new FileInputStream(filetoupload);
			((ChannelSftp) sftpChannel).put(in, filetoupload.getName(), ChannelSftp.OVERWRITE);
			logger.trace(CLASS_NAME + " uploadFileToSFTP(): File moved to SFTP location");
		} catch (Exception e) {
			logger.error(CLASS_NAME + "uploadFileToSFTP(): Exception occurred while provisioning ", e);
			throw e;
		} finally {
			try {
				sftpChannel.disconnect();
				in.close();
			} catch (Exception e) {
			}
		}
		logger.info(CLASS_NAME + " uploadFileToSFTP(): End of method");
	}

	public void uploadFileToFTP(String filename, String destination) throws Exception {
		logger.info(CLASS_NAME + " uploadFileToFTP(): Start of method");
		FileInputStream in = null;
		FTPClient ftp = IDEMIAClientHelper.getFTPConnection(_ftphostname, _ftpusername, _ftppassword, _ftpportnumber, _isSSHAuthentication);
		try {
			File filetoupload = new File(filename);
			in = new FileInputStream(filetoupload);
			ftp.storeFile(filetoupload.getName(), in);
			logger.trace(CLASS_NAME + " uploadFileToFTP(): File moved successfully");
		} catch (Exception e) {
			logger.error(CLASS_NAME + " uploadFileToFTP(): Exception occurred " + e);
			throw e;
		} finally {
			try {
				if (in != null)
					in.close();
				if (ftp != null) {
					ftp.logout();
					ftp.disconnect();
				}
			} catch (Exception e) {
			}
		}
		logger.info(CLASS_NAME + " uploadFileToFTP(): End of method");
	}

	@SuppressWarnings("rawtypes")
	String getActualValueFromMap(Map parameters, String property) {
		String result = "";
		if (parameters.get(property) != null && !parameters.get(property).toString().isEmpty()) {
			result = parameters.get(property).toString();
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	private String getValueFromMap(Map parameters, String property) {
		String result = "";
		result = getActualValueFromMap(parameters, property);
		if (dateFieldsMap.keySet().contains(property)) {
			String dateFormattoCOnverted = dateFieldsMap.get(property);
			try {
				result = parseDateStringToDateString(result, _alertAppDateFormat, dateFormattoCOnverted);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (property.equalsIgnoreCase(IDEMIAConnectorConstants.FIRST_NAME)) {
			result = prepareFirstNameWithFontInfo(result, parameters);
		} else if (property.equalsIgnoreCase(IDEMIAConnectorConstants.LAST_NAME)) {
			result = prepareLastNameWithFontInfo(result, parameters);
		}

		return result;
	}

	@SuppressWarnings("rawtypes")
	private String prepareLastNameWithFontInfo(String result, Map parameters) {
		String lastNameFontsize = getActualValueFromMap(parameters, IDEMIAConnectorConstants.LAST_NAME_FONT);
		if (lastNameFontsize != "") {
			result = result.concat(IDEMIAConnectorConstants.HASH_DELIMITER).concat(lastNameFontsize).toString();
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	private String prepareFirstNameWithFontInfo(String result, Map parameters) {
		String firstNameFontsize = getActualValueFromMap(parameters, IDEMIAConnectorConstants.FIRST_NAME_FONT);
		if (!firstNameFontsize.isEmpty()) {
			result = result.concat(IDEMIAConnectorConstants.HASH_DELIMITER).concat(firstNameFontsize).toString();
		}
		String firstNameThirdline = getActualValueFromMap(parameters, IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE);
		if (!firstNameThirdline.isEmpty()) {
			result = result.concat(IDEMIAConnectorConstants.AND_DELIMITER).concat(firstNameThirdline).toString();
			String firstNameThirdlineFontsize = getActualValueFromMap(parameters, IDEMIAConnectorConstants.FIRST_NAME_THIRD_LINE_FONT);
			if (!firstNameThirdlineFontsize.isEmpty()) {
				result = result.concat(IDEMIAConnectorConstants.HASH_DELIMITER).concat(firstNameThirdlineFontsize).toString();
			}
		}
		return result;
	}

	public void IncrementAndFormatrecordAndRecordSet(PrintStream fileps) {
		fileps.format("%1$08d", record);
		fileps.print(_delimiter);
		fileps.format("%1$08d", recordSetId);
		fileps.print(_delimiter);
		record++;
	}

	public void IncrementAndFormatRecordNumber(PrintStream fileps) {
		fileps.format("%1$08d", record);
		fileps.print(_delimiter);
		record++;
	}

	@SuppressWarnings("rawtypes")
	public void genHeader(Map paramters, PrintStream fileps) {
		IncrementAndFormatRecordNumber(fileps);
		fileps.print(getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_HEADER_LABEL) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.FILE_DATE) + _delimiter);
		fileps.print(getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_RECIPIENT_NAME) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.PHONE_NUMBER) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_STREET1) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_STREET2) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_CITY) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_STATE) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_ZIP) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_ZIP_SFX) + _delimiter);
		fileps.println(getValueFromMap(paramters, IDEMIAConnectorConstants.CARD_REQUEST_COUNT));
	}

	@SuppressWarnings("rawtypes")
	public void genAlternativeHeader(Map paramters, PrintStream fileps) {
		IncrementAndFormatRecordNumber(fileps);
		fileps.print(IDEMIAConnectorConstants.SHIPPING_RECORD + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_RECIPIENT_NAME) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.ALTERNATE_PHONE_NUMBER) + _delimiter);
		fileps.print(getValueFromMap(paramters, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET1) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STREET2) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_CITY) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_STATE) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.ALTERNATE_SHIPPING_ZIP_SFX) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_MODE) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.SHIPPING_TYPE) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.CARDTYPE) + _delimiter);
		fileps.println(getValueFromMap(paramters, IDEMIAConnectorConstants.SLA_TURNAROUND_TIME));
	}

	@SuppressWarnings("rawtypes")
	public void genFront(Map paramters, PrintStream fileps) {
		// IncrementAndFormatrecordAndRecordSet(fileps);
		IncrementAndFormatRecordNumber(fileps);
		fileps.println(
				recordSetId + _delimiter + IDEMIAConnectorConstants.CARD_FRONT_CODE + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.PICTURE) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.LAST_NAME)
						+ _concatDelimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.FIRST_NAME) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.SIGNATURE_FIELD) + _delimiter
						+ getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_SPECIFIC_TEXT) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.RANK) + _delimiter
						+ getValueFromMap(paramters, IDEMIAConnectorConstants.PDF417_2D_BARCODE) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.EMPLOYEE_AFFILIATION) + _delimiter
						+ getValueFromMap(paramters, IDEMIAConnectorConstants.HEADER) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_LINE1) + _concatDelimiter
						+ getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_LINE2) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.SEAL) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.FOOTER)
						+ _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.ISSUE_DATE) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.EXPIRATION_DATE) + _delimiter
						+ getValueFromMap(paramters, IDEMIAConnectorConstants.COLOR_BAR_CODING) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.COLOR_BORDER_CODING) + _delimiter
						+ getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT1) + _concatDelimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT2) + _concatDelimiter
						+ getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_SPECIFIC_FRONT_TEXT3) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.AFFILIATION_COLOR_CODE) + _delimiter
						+ getValueFromMap(paramters, IDEMIAConnectorConstants.EXPIRATION_DATE_FIPS_201_2) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.ORGANIZATIONAL_AFFILIATION));
	}

	@SuppressWarnings("rawtypes")
	public void genBack(Map paramters, PrintStream fileps) {
		// IncrementAndFormatrecordAndRecordSet(fileps);
		IncrementAndFormatRecordNumber(fileps);
		fileps.println(recordSetId + _delimiter + IDEMIAConnectorConstants.CARD_BACK_CODE + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_CARD_SERIAL) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.ISSUER_IDENTIFICATION) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.MAG_STRIPE_TRACK1) + _concatDelimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.MAG_STRIPE_TRACK2) + _concatDelimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.MAG_STRIPE_TRACK3) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.RETURN_ADDRESS_LINE1) + _concatDelimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.RETURN_ADDRESS_LINE2) + _concatDelimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.RETURN_ADDRESS_LINE3) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.HEIGHT) + _concatDelimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.EYES) + _concatDelimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.HAIR) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.THREE_OF_9_BAR_CODE) + _delimiter + getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1) + _delimiter
				+ getValueFromMap(paramters, IDEMIAConnectorConstants.AGENCY_SPECIFIC_BACK_TEXT1));
	}

	private String parseDateStringToDateString(String dateString, String currentFormat, String exceptedFormat) throws Exception {
		try {
			DateTimeFormatter oldPattern = DateTimeFormatter.ofPattern(currentFormat);
			DateTimeFormatter newPattern = DateTimeFormatter.ofPattern(exceptedFormat);
			LocalDateTime datetime = LocalDateTime.parse(dateString, oldPattern);
			return datetime.format(newPattern).toUpperCase();
		} catch (Exception e) {
			throw e;
		}
	}

	static String getAlphaNumericString(int n) {

		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvxyz";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (AlphaNumericString.length() * Math.random());
			sb.append(AlphaNumericString.charAt(index));
		}
		return sb.toString();
	}

	private boolean downloadFilesFromFTP(String ftpFileLocation, String localLocation) {
		String methodName = "downloadFilesFromFTP";
		String ftpFileName = null;
		String localFileName = null;
		String ftpFilePath = null;
		boolean isSuccess = false;
		OutputStream op = null;
		FTPClient ftp = null;
		try {
			ftp = IDEMIAClientHelper.getFTPConnection(_ftphostname, _ftpusername, _ftppassword, _ftpportnumber, _isSSHAuthentication);
			ftpFilePath = ftpFileLocation;
			ftp.changeWorkingDirectory(ftpFilePath);
			FTPFile[] ftpFiles = ftp.listFiles();
			for (FTPFile ftpFile : ftpFiles) {
				if (!ftpFile.isDirectory()) {
					ftpFileName = ftpFile.getName();
					ftp.changeWorkingDirectory(ftpFilePath);
					localFileName = ftpFileName;
					op = new FileOutputStream(localLocation + File.separator + localFileName);
					isSuccess = ftp.retrieveFile(ftpFileName, op);
					logger.debug(CLASS_NAME + methodName + "Successfully Downloaded File-->" + localFileName + " from FTP location-->" + ftpFilePath + "/" + ftpFileName + " to local location-->" + localLocation + "/" + localFileName);
					op.close();
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " " + methodName + " " + "downloadFilesFromFTP() exception--> :", e);
			isSuccess = false;
		} finally {

			try {
				if (ftp != null) {
					ftp.logout();
					ftp.disconnect();
				}

				if (op != null) {
					op.close();

				}
			} catch (Exception e) {
				logger.error(CLASS_NAME + " " + methodName + " " + "downloadFilesFromFTP()- error in closing ftp or outputStream:", e);
			}

		}
		return isSuccess;
	}

	@SuppressWarnings("unchecked")
	private boolean downloadFilesFromSFTP(String sftpFileLocation, String localLocation) {
		String methodName = "downloadFilesFromSFTP";
		String localFileName = null;
		String sftpFilePath = null;
		boolean isSuccess = false;
		ChannelSftp channel = null;

		try {
			logger.debug(CLASS_NAME + methodName + " Start of method");
			Channel sftpChannel = IDEMIAClientHelper.getSFTPConnection(_ftphostname, _ftpusername, _ftppassword, _ftpportnumber, _privateKeyFilePath, _passphrase, _isSSHAuthentication);
			channel = ((ChannelSftp) sftpChannel);
			if (channel == null || channel.isClosed())
				throw new Exception("Login Failed, Please check SFTP Login credentials!!");
			logger.trace(CLASS_NAME + methodName + "Dowloading the files from location: " + sftpFileLocation);
			sftpFilePath = sftpFileLocation;
			logger.trace(CLASS_NAME + methodName + "sftpFilePath  :: " + sftpFilePath);
			logger.trace(CLASS_NAME + methodName + "localLocation   :: " + localLocation);
			channel.cd(sftpFilePath);
			logger.trace(CLASS_NAME + methodName + "Changed the directories");
			Vector<ChannelSftp.LsEntry> list = channel.ls(sftpFilePath);
			if (list != null) {
				logger.trace(CLASS_NAME + methodName + "Number of files/directories recieved:" + list.size());
				for (ChannelSftp.LsEntry entry : list) {
					if (entry != null && !entry.getAttrs().isDir()) {
						localFileName = entry.getFilename();
						channel.get(entry.getFilename(), localLocation + File.separator + localFileName);
						logger.debug(CLASS_NAME + methodName + "Successfully downloaded File-->" + localFileName + " from SFTP location-->" + sftpFilePath + "/" + entry.getFilename() + " to local location-->" + localLocation + "/"
								+ localFileName);
					}
				}
			} else {
				logger.trace(CLASS_NAME + methodName + "No Directories/Files found at SFTP location: " + sftpFilePath);
			}
			isSuccess = true;
		} catch (Exception e) {
			logger.error(CLASS_NAME + " " + methodName + " " + "downloadFilesFromSFTP() exception--> :", e);
			isSuccess = false;
		} finally {
			if (channel != null) {
				channel.disconnect();
				try {
					channel.getSession().disconnect();
				} catch (JSchException e) {
				}
			}
		}

		return isSuccess;

	}

	@SuppressWarnings("unchecked")
	public void deleteFilesFromSFTPLocation(String sftpFileLocation) {
		String methodName = "removeFilesFromFTP";
		ChannelSftp channel = null;
		try {
			logger.debug(CLASS_NAME + methodName + " Start of method");
			Channel sftpChannel = IDEMIAClientHelper.getSFTPConnection(_ftphostname, _ftpusername, _ftppassword, _ftpportnumber, _privateKeyFilePath, _passphrase, _isSSHAuthentication);
			channel = ((ChannelSftp) sftpChannel);
			if (channel == null || channel.isClosed())
				throw new Exception("Login Failed, Please check SFTP Login credentials!!");
			// In case path is directly pointing to csv file path on ftp server
			if (sftpFileLocation.endsWith(".RET")) {
				logger.trace(CLASS_NAME + methodName + "Removing the files from location: " + sftpFileLocation);
				channel.rm(sftpFileLocation);
				logger.debug(CLASS_NAME + methodName + " Successfully Removed File from SFTP location-->" + sftpFileLocation);
			} else {// In case path is pointing to folder containing csv files on ftp server
				logger.trace(CLASS_NAME + methodName + "Removing the file(s) from location: " + sftpFileLocation);
				channel.cd(sftpFileLocation);
				logger.trace(CLASS_NAME + methodName + "Changed the directories");
				Vector<ChannelSftp.LsEntry> list = channel.ls(sftpFileLocation);
				if (list != null) {
					logger.trace(CLASS_NAME + methodName + "Number of files/directories recieved:" + list.size());
					for (ChannelSftp.LsEntry entry : list) {
						if (entry != null && !entry.getAttrs().isDir()) {
							channel.rm(entry.getFilename());
							// writeFile(channel, entry.getFilename(), localLocation + File.separator + localFileName);
							logger.debug(CLASS_NAME + methodName + "Removed file from SFTP location-->" + sftpFileLocation + "/" + entry.getFilename());
						}
					}
				} else {
					logger.trace(CLASS_NAME + methodName + "No Directories/Files found at SFTP location: " + sftpFileLocation);
				}

			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " " + methodName + " error removing the file from  :" + sftpFileLocation, e);

		} finally {
			if (channel != null) {
				channel.disconnect();
				try {
					channel.getSession().disconnect();
				} catch (JSchException e) {
				}
			}
		}
	}

}
