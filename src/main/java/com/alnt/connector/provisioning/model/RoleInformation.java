package com.alnt.connector.provisioning.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alnt.extractionconnector.common.model.IRoleInformation;

public class RoleInformation implements IRoleInformation,com.alnt.fabric.component.rolemanagement.search.IRoleInformation {

	private String name;
	private String repositoryRoleName;
	private Date validFrom;
	private Date validTo;
	private String validFromMappingAttr;
	private String validToMappingAttr;
	private String description;
	private String longDescription;
	private Map<String, List<String>> memberData;
	private String rbacRoleType;
	private String language;
	private String enterpriseRoleName;
	private String roleType;
	private String roleAction;

	@Override
	public String getRoleAction() {
		return roleAction;
	}
	@Override
	public void setRoleAction(final String roleAction) {
		this.roleAction = roleAction;
	}
	@Override
	public String getName() {
		return name;
	}
	@Override
	public void setName(final String name) {
		this.name = name;
	}
	@Override
	public String getRepositoryRoleName() {
		return repositoryRoleName;
	}
	@Override
	public void setRepositoryRoleName(final String repositoryRoleName) {
		this.repositoryRoleName = repositoryRoleName;
	}
	@Override
	public Date getValidFrom() {
		return validFrom;
	}
	@Override
	public void setValidFrom(final Date validFrom) {
		this.validFrom = validFrom;
	}
	@Override
	public Date getValidTo() {
		return validTo;
	}
	@Override
	public void setValidTo(final Date validTo) {
		this.validTo = validTo;
	}
	@Override
	public String getValidFromMappingAttr() {
		return validFromMappingAttr;
	}
	@Override
	public void setValidFromMappingAttr(final String validFromMappingAttr) {
		this.validFromMappingAttr = validFromMappingAttr;
	}
	@Override
	public String getValidToMappingAttr() {
		return validToMappingAttr;
	}
	@Override
	public void setValidToMappingAttr(final String validToMappingAttr) {
		this.validToMappingAttr = validToMappingAttr;
	}
	@Override
	public String getDescription() {
		return description;
	}
	@Override
	public void setDescription(final String description) {
		this.description = description;
	}
	@Override
	public String getLongDescription() {
		return longDescription;
	}
	@Override
	public void setLongDescription(final String longDescription) {
		this.longDescription = longDescription;
	}
	@Override
	public Map<String, List<String>> getMemberData() {
		return memberData;
	}
	@Override
	public void setMemberData(final Map<String, List<String>> memberData) {
		this.memberData = memberData;
	}
	public String getRbacRoleType() {
		return rbacRoleType;
	}
	public void setRbacRoleType(final String rbacRoleType) {
		this.rbacRoleType = rbacRoleType;
	}
	@Override
	public String getLanguage() {
		return language;
	}
	@Override
	public void setLanguage(final String language) {
		this.language = language;
	}
	@Override
	public String getEnterpriseRoleName() {
		return enterpriseRoleName;
	}
	@Override
	public void setEnterpriseRoleName(final String enterpriseRoleName) {
		this.enterpriseRoleName = enterpriseRoleName;
	}
	@Override
	public String getRoleType() {
		return roleType;
	}
	@Override
	public void setRoleType(final String roleType) {
		this.roleType = roleType;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RoleInformation [name=").append(name).append(", repositoryRoleName=").append(repositoryRoleName)
		.append(", validFrom=").append(validFrom).append(", validTo=").append(validTo)
		.append(", validFromMappingAttr=").append(validFromMappingAttr).append(", validToMappingAttr=")
		.append(validToMappingAttr).append(", description=").append(description).append(", longDescription=")
		.append(longDescription).append(", memberData=").append(memberData).append(", rbacRoleType=")
		.append(rbacRoleType).append(", language=").append(language).append(", enterpriseRoleName=")
		.append(enterpriseRoleName).append(", roleType=").append(roleType).append(", roleAction=")
		.append(roleAction).append("]");
		return builder.toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((enterpriseRoleName == null) ? 0 : enterpriseRoleName.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((longDescription == null) ? 0 : longDescription.hashCode());
		result = prime * result + ((memberData == null) ? 0 : memberData.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((rbacRoleType == null) ? 0 : rbacRoleType.hashCode());
		result = prime * result + ((repositoryRoleName == null) ? 0 : repositoryRoleName.hashCode());
		result = prime * result + ((roleAction == null) ? 0 : roleAction.hashCode());
		result = prime * result + ((roleType == null) ? 0 : roleType.hashCode());
		result = prime * result + ((validFrom == null) ? 0 : validFrom.hashCode());
		result = prime * result + ((validFromMappingAttr == null) ? 0 : validFromMappingAttr.hashCode());
		result = prime * result + ((validTo == null) ? 0 : validTo.hashCode());
		result = prime * result + ((validToMappingAttr == null) ? 0 : validToMappingAttr.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleInformation other = (RoleInformation) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (enterpriseRoleName == null) {
			if (other.enterpriseRoleName != null)
				return false;
		} else if (!enterpriseRoleName.equals(other.enterpriseRoleName))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (longDescription == null) {
			if (other.longDescription != null)
				return false;
		} else if (!longDescription.equals(other.longDescription))
			return false;
		if (memberData == null) {
			if (other.memberData != null)
				return false;
		} else if (!memberData.equals(other.memberData))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rbacRoleType == null) {
			if (other.rbacRoleType != null)
				return false;
		} else if (!rbacRoleType.equals(other.rbacRoleType))
			return false;
		if (repositoryRoleName == null) {
			if (other.repositoryRoleName != null)
				return false;
		} else if (!repositoryRoleName.equals(other.repositoryRoleName))
			return false;
		if (roleAction == null) {
			if (other.roleAction != null)
				return false;
		} else if (!roleAction.equals(other.roleAction))
			return false;
		if (roleType == null) {
			if (other.roleType != null)
				return false;
		} else if (!roleType.equals(other.roleType))
			return false;
		if (validFrom == null) {
			if (other.validFrom != null)
				return false;
		} else if (!validFrom.equals(other.validFrom))
			return false;
		if (validFromMappingAttr == null) {
			if (other.validFromMappingAttr != null)
				return false;
		} else if (!validFromMappingAttr.equals(other.validFromMappingAttr))
			return false;
		if (validTo == null) {
			if (other.validTo != null)
				return false;
		} else if (!validTo.equals(other.validTo))
			return false;
		if (validToMappingAttr == null) {
			if (other.validToMappingAttr != null)
				return false;
		} else if (!validToMappingAttr.equals(other.validToMappingAttr))
			return false;
		return true;
	}

}
