package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alnt.extractionconnector.common.model.IUserInformation;

public class UserInformation implements IUserInformation, Serializable{

	private static final long serialVersionUID = 1141010807826631140L;
	private final Set<String> exclustionFields = new HashSet<String>(Arrays.asList("LastUpdatedTime"));
	
	private Map<String, Object> userDetails;
	private Map<String, Map<String, List<Map<String, Object>>>> entitlements;
	
	public Map<String, Object> getUserDetails() {
		return userDetails;
	}

	public Map<String, Map<String, List<Map<String, Object>>>> getEntitlements() {
		return entitlements;
	}
	
	public void setUserDetails( Map<String, Object>  userDetails){
		this.userDetails=userDetails;
	}

	public void setEntitlements(
			Map<String, Map<String, List<Map<String, Object>>>> entitlements) {
		this.entitlements=entitlements;		
	}
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("ReconUserInformation [");
		if(userDetails != null && !userDetails.isEmpty()) {
			Iterator<String> keys = userDetails.keySet().iterator();
			str.append("userDetails= [");
			while(keys.hasNext()) {
				String key = keys.next();
				Object value = userDetails.get(key);
				if(!exclustionFields.contains(key)) {
					str.append(key).append(" = ");
					str.append(value);
				str.append(",");
				}
			}
			str.append("]");
		}
		str.append(", entitlements= ").append(entitlements).append("]");
		
		return str.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entitlements == null) ? 0 : entitlements.hashCode());
		result = prime * result + ((userDetails == null) ? 0 : userDetails.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInformation other = (UserInformation) obj;
		if (entitlements == null) {
			if (other.entitlements != null)
				return false;
		} else if (!entitlements.equals(other.entitlements))
			return false;
		if (userDetails == null) {
			if (other.userDetails != null)
				return false;
		} else if (!userDetails.equals(other.userDetails))
			return false;
		return true;
	}
	
	
}
